package com.tsystems.js.sbb.repository.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.repository.interfaces.TimetableDao;

@Repository
public class TimetableDaoImpl implements TimetableDao {

	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void save(Timetable timetable) {
		sessionFactory.getCurrentSession().save(timetable);
	}

	@Override
	public List<Timetable> getTimetablesByStationNameAndDate(String station, LocalDate date) {
		@SuppressWarnings("unchecked")
		TypedQuery<Timetable> query = sessionFactory.getCurrentSession().createQuery(
				"select t from Timetable t inner join t.station st where st.stationName = :station and (t.departureTime between :time1 and :time2 or t.arrivalTime between :time1 and :time2)");
		query.setParameter("station", station);
		query.setParameter("time1", date.atStartOfDay());
		query.setParameter("time2", date.atTime(23, 59));
		return query.getResultList();
	}
	
	@Override
	public List<Timetable> getTimetablesByStationNameAndCurrentTime(String station) {
		@SuppressWarnings("unchecked")
		TypedQuery<Timetable> query = sessionFactory.getCurrentSession().createQuery(
				"select t from Timetable t inner join t.station st where st.stationName = :station and (t.departureTime between :time1 and :time2 or t.arrivalTime between :time1 and :time2)");
		query.setParameter("station", station);
		query.setParameter("time1", LocalDateTime.now());
		query.setParameter("time2", LocalDate.now().atTime(23, 59));
		return query.getResultList();
	}
}
