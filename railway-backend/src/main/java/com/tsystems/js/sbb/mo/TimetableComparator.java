package com.tsystems.js.sbb.mo;

import java.util.Comparator;

import com.tsystems.js.sbb.service.dto.TimetableDto;

public class TimetableComparator implements Comparator<TimetableDto> {

	public int compare(TimetableDto a, TimetableDto b) {

		if (a.getDepartureTime() != null && b.getDepartureTime() != null && a.getArrivalTime() != null
				&& b.getArrivalTime() != null) {
			int currentResult = a.getArrivalTime().compareTo(b.getArrivalTime());
			if (currentResult == 0) {
				return a.getDepartureTime().compareTo(b.getDepartureTime());
			}
			else {
				return currentResult;
			}
		}

		if (a.getArrivalTime() == null && a.getDepartureTime() != null && b.getArrivalTime() == null
				&& b.getDepartureTime() != null) {
			return a.getDepartureTime().compareTo(b.getDepartureTime());
		}

		if (a.getArrivalTime() != null && a.getDepartureTime() == null && b.getArrivalTime() != null
				&& b.getDepartureTime() == null) {
			return a.getArrivalTime().compareTo(b.getArrivalTime());
		}

		if ((a.getArrivalTime() != null && a.getDepartureTime() != null && b.getArrivalTime() == null
				&& b.getDepartureTime() != null)
				|| (a.getArrivalTime() == null && a.getDepartureTime() != null && b.getArrivalTime() != null
						&& b.getDepartureTime() != null)) {
			return a.getDepartureTime().compareTo(b.getDepartureTime());
		}

		if ((a.getArrivalTime() != null && a.getDepartureTime() != null && b.getArrivalTime() != null
				&& b.getDepartureTime() == null)
				|| (a.getArrivalTime() != null && a.getDepartureTime() == null && b.getArrivalTime() != null
						&& b.getDepartureTime() != null)) {
			return a.getArrivalTime().compareTo(b.getArrivalTime());
		}

		if (a.getArrivalTime() == null && a.getDepartureTime() != null && b.getArrivalTime() != null
				&& b.getDepartureTime() == null) {
			return a.getDepartureTime().compareTo(b.getArrivalTime());
		}

		if (a.getArrivalTime() != null && a.getDepartureTime() == null && b.getArrivalTime() == null
				&& b.getDepartureTime() != null) {
			return a.getArrivalTime().compareTo(b.getDepartureTime());
		}
		return 0;
	}
}
