package com.tsystems.js.sbb.repository.interfaces;

import java.util.List;

import com.tsystems.js.sbb.entity.Station;

public interface StationDao {
	void save(Station station);

	List<Station> getAll();

	Station getStationByName(String stationName);
}
