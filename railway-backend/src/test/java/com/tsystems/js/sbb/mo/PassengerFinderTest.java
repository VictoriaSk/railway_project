package com.tsystems.js.sbb.mo;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.js.sbb.entity.Passenger;
import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.exceptions.PassengersNotFoundException;
import com.tsystems.js.sbb.exceptions.TrainMissingException;
import com.tsystems.js.sbb.repository.impl.PassengerDaoImpl;
import com.tsystems.js.sbb.repository.impl.TrainDaoImpl;
import com.tsystems.js.sbb.repository.interfaces.PassengerDao;
import com.tsystems.js.sbb.repository.interfaces.TrainDao;
import com.tsystems.js.sbb.service.dto.ClientSearchRequest;
import com.tsystems.js.sbb.service.dto.PassengerDto;

public class PassengerFinderTest {
	private PassengerFinder sut;
	private ClientSearchRequest request;
	private TrainDao trainDaoMock;
	private PassengerDao passengerDaoMock;
	private String trainNumber;
	private LocalDate departureDate;
	
	@Before
	public void setUp() {
		trainNumber = "100";
		departureDate = LocalDate.now();
		trainDaoMock = Mockito.mock(TrainDaoImpl.class);
		passengerDaoMock = Mockito.mock(PassengerDaoImpl.class);
		sut = new PassengerFinder(passengerDaoMock, trainDaoMock);
	}

	@Test
	public void testSearchSuccessSomePassengers() { 
		List <Train> chosenTrain = List.of(new Train("100", 500));
		Mockito.when(trainDaoMock.getTrainsByRouteNumberAndDepartureDate(trainNumber, departureDate))
				.thenReturn(chosenTrain);
		Passenger firstPassenger = new Passenger("First", "First", LocalDate.now());
		Passenger secondPassenger = new Passenger("Second", "Second", LocalDate.now());
		Passenger nextPassenger = new Passenger("Next", "Next", LocalDate.now());
		Mockito.when(passengerDaoMock.getPassengersByTrainId(chosenTrain.get(0).getTrainId())).thenReturn(List.of(firstPassenger, secondPassenger, nextPassenger));
		request = new ClientSearchRequest();
		request.setDepartureDate(departureDate);
		request.setTrainNumber(trainNumber);
		List <PassengerDto> resultList = (List<PassengerDto>) sut.search(request);
		assertEquals(firstPassenger.getName(), resultList.get(0).getName());
		assertEquals(firstPassenger.getSurname(), resultList.get(0).getSurname());
		assertEquals(secondPassenger.getName(), resultList.get(2).getName());
		assertEquals(secondPassenger.getSurname(), resultList.get(2).getSurname());
		assertEquals(nextPassenger.getName(), resultList.get(1).getName());
		assertEquals(nextPassenger.getSurname(), resultList.get(1).getSurname());
	}
	
	@Test
	public void testSearchSuccessOnePassenger() { 
		List <Train> chosenTrain = List.of(new Train("100", 500));
		Mockito.when(trainDaoMock.getTrainsByRouteNumberAndDepartureDate(trainNumber, departureDate))
				.thenReturn(chosenTrain);
		Passenger firstPassenger = new Passenger("First", "First", LocalDate.now());
		Mockito.when(passengerDaoMock.getPassengersByTrainId(chosenTrain.get(0).getTrainId())).thenReturn(List.of(firstPassenger));
		request = new ClientSearchRequest();
		request.setDepartureDate(departureDate);
		request.setTrainNumber(trainNumber);
		List <PassengerDto> resultList = (List<PassengerDto>) sut.search(request);
		assertEquals(firstPassenger.getName(), resultList.get(0).getName());
		assertEquals(firstPassenger.getSurname(), resultList.get(0).getSurname());
		assertEquals(1, resultList.size());
	}
	
	@Test
	public void testSearchTrainMissing() { 
		Mockito.when(trainDaoMock.getTrainsByRouteNumberAndDepartureDate(trainNumber, departureDate))
		.thenReturn(Collections.emptyList());
		request = new ClientSearchRequest();
		request.setDepartureDate(departureDate);
		request.setTrainNumber(trainNumber);
		try {
		sut.search(request);
		} catch (TrainMissingException ex) {
			// test passed
		}
	}
	
	@Test
	public void testSearchPassengersNotFound() { 
		List <Train> chosenTrain = List.of(new Train("100", 500));
		Mockito.when(trainDaoMock.getTrainsByRouteNumberAndDepartureDate(trainNumber, departureDate))
		.thenReturn(chosenTrain);
		Mockito.when(passengerDaoMock.getPassengersByTrainId(chosenTrain.get(0).getTrainId())).thenReturn(Collections.emptyList());
		request = new ClientSearchRequest();
		request.setDepartureDate(departureDate);
		request.setTrainNumber(trainNumber);
		try {
		sut.search(request);
		} catch (PassengersNotFoundException ex) {
			// test passed
		}
	}
}
