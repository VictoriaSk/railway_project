package com.tsystems.js.sbb.service.dto;

import java.util.Collection;

public class BuyTicketResponse {
    private TicketDto ticket;
    private Collection<Message> messages;


    public BuyTicketResponse(Collection<Message> messages) {
        this.messages = messages;
    }

    public BuyTicketResponse(TicketDto ticket, Collection<Message> messages) {
        this.ticket = ticket;
        this.messages = messages;
    }

    public TicketDto getTicket() {
        return ticket;
    }

    public Collection<Message> getMessages() {
        return messages;
    }
}

