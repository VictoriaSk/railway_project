package com.tsystems.js.sbb.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "com.tsystems.js.sbb"})
public class AppSpringConfig {
	
}
