package com.tsystems.js.sbb.jsf;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tsystems.js.sbb.service.dto.AllStationsResponse;
import com.tsystems.js.sbb.service.dto.BuyTicketRequest;
import com.tsystems.js.sbb.service.dto.BuyTicketResponse;
import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;
import com.tsystems.js.sbb.service.dto.StationDto;
import com.tsystems.js.sbb.service.dto.TrainDto;
import com.tsystems.js.sbb.service.dto.TrainSearchRequest;
import com.tsystems.js.sbb.service.dto.TrainSearchResponse;
import com.tsystems.js.sbb.service.interfaces.StationService;
import com.tsystems.js.sbb.service.interfaces.TicketService;
import com.tsystems.js.sbb.service.interfaces.TrainService;

@Component
@RequestScoped
@ManagedBean
public class ClientBean {

	private TicketService ticketService;
	private TrainService trainService;
	private StationService stationService;

	@ManagedProperty("#{param.depStation}")
	private String departureStation;

	@ManagedProperty("#{param.arrStation}")
	private String arrivalStation;

	@ManagedProperty("#{param.travelDate}")
	private String travelDate;

	@ManagedProperty("#{param.name}")
	private String name;

	@ManagedProperty("#{param.name}")
	private String surname;

	@ManagedProperty("#{param.dateOfBirth}")
	private String dateOfBirth;

	private boolean flag;

	private List<UiTrainBean> trainList;

	private List<TrainDto> trainDtoList;

	private String error;

	private String departureDate;

	private String trainNumber;

	private UiTrainBean selectedTrain;

	private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss zzz uuuu", Locale.ROOT);

	public ClientBean() {

	}

	@Autowired
	public void setTicketService(TicketService ticketService) {
		this.ticketService = ticketService;
	}

	@Autowired
	public void setTrainService(TrainService trainService) {
		this.trainService = trainService;
	}

	@Autowired
	public void setStationService(StationService stationService) {
		this.stationService = stationService;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public String getArrivalStation() {
		return arrivalStation;
	}

	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	public String getTravelDate() {
		return travelDate;
	}

	public void setTravelDate(String travelDate) {
		this.travelDate = travelDate;
	}

	public List<UiTrainBean> getTrainList() {
		return trainList;
	}

	public void setTrainList(List<UiTrainBean> trainList) {
		this.trainList = trainList;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}

	public UiTrainBean getSelectedTrain() {
		return selectedTrain;
	}

	public void setSelectedTrain(UiTrainBean selectedTrain) {
		this.selectedTrain = selectedTrain;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String saveSelectedTrain() {
		return "searchTrains";
	}

	public List<String> displayStations(String query) {
		AllStationsResponse stationsResponse = stationService.displayAllStations();
		List<String> results = new ArrayList<>();
		if (stationsResponse.getMessages().isEmpty()) {
			List<StationDto> stationList = (List<StationDto>) stationsResponse.getStations();
			stationList.forEach(station -> {
				if (StringUtils.startsWith(station.getStationName(), query)) {
					results.add(station.getStationName());
				}
			});

		}
		return results;
	}

	public String searchTrains() {
		error = "";
		TrainSearchRequest trainSearchRequest = new TrainSearchRequest();
		trainSearchRequest.setFrom(departureStation);
		trainSearchRequest.setTo(arrivalStation);
		trainSearchRequest.setTravelDate(LocalDate.parse(travelDate, dateFormatter));
		TrainSearchResponse response = trainService.searchTrains(trainSearchRequest);
		if (response.getMessages().isEmpty()) {
			trainDtoList = (List<TrainDto>) response.getTrains();
			trainList = LocalDateTimeToStringConverter.convertTrainDateTime(trainDtoList);
		} else {
			for (Message message : response.getMessages()) {
				if (message.type.equals(MessageType.INFO)) {
					error = message.getText();
				} else {
					error = "Some technical error has occured. Please try again later.";
				}
			}
		}
		return "searchTrains";
	}

	public String buyTicket() {
		error = "";
		BuyTicketRequest buyTicketRequest = new BuyTicketRequest();
		buyTicketRequest.setName(name);
		buyTicketRequest.setSurname(surname);
		buyTicketRequest.setDateOfBirth(LocalDate.parse(dateOfBirth, dateFormatter));
		buyTicketRequest.setTrainNumber(selectedTrain.getRouteNum());
		for (TrainDto train : trainDtoList) {
			if (train.getRouteNum().equals(selectedTrain.getRouteNum())) {
				buyTicketRequest.setDepartureDate(train.getDate());
			}
		}
		BuyTicketResponse response = ticketService.buyTicket(buyTicketRequest);
		if (response.getMessages().isEmpty()) {
			flag = true;
		} else {
			response.getMessages().forEach(message -> {
				if (message.type.equals(MessageType.INFO)) {
					error = message.getText();
				} else {
					error = "Some technical error occured. Please try again later.";
				}
			});
		}
		return "searchTrains";
	}

	public String refreshPage() {
		flag = false;
		departureDate = StringUtils.EMPTY;
		trainNumber = StringUtils.EMPTY;
		name = StringUtils.EMPTY;
		departureStation = StringUtils.EMPTY;
		arrivalStation = StringUtils.EMPTY;
		surname = StringUtils.EMPTY;
		dateOfBirth = StringUtils.EMPTY;
		travelDate = StringUtils.EMPTY;
		selectedTrain.setRouteNum(StringUtils.EMPTY);
		return "searchTrains";
	}
}
