package com.tsystems.js.sbb.exceptions;

import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;

public class TrainsNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private Message message = new Message ("Unfortunately, there are no trains suitable for your journey", MessageType.INFO);

	public String getMessage() {
		return message.getText();
	}
}
