package com.tsystems.js.sbb.jsf;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tsystems.js.sbb.service.dto.AllStationsResponse;
import com.tsystems.js.sbb.service.dto.MessageType;
import com.tsystems.js.sbb.service.dto.StationDto;
import com.tsystems.js.sbb.service.dto.StationTimetableRequest;
import com.tsystems.js.sbb.service.dto.StationTimetableResponse;
import com.tsystems.js.sbb.service.dto.TimetableDto;
import com.tsystems.js.sbb.service.interfaces.StationService;
import com.tsystems.js.sbb.service.interfaces.TimetableService;

@Component
@RequestScoped
@ManagedBean
public class InfoBoardBean {

	private TimetableService timetableService;
	private StationService stationService;

	private List<StationDto> stationList;
	private String error;
	private List<TimetableDto> timetables;
	private List<UiTimetableBean> trainList;

	@ManagedProperty(value = "stName")
	private String stationName;

	@ManagedProperty(value = "date")
	private String date;

	public InfoBoardBean() {
	}

	@Autowired
	public void setTimetableService(TimetableService timetableService) {
		this.timetableService = timetableService;
	}

	@Autowired
	public void setStationService(StationService stationService) {
		this.stationService = stationService;
	}

	public void setTrainList(List<UiTimetableBean> trainList) {
		this.trainList = trainList;
	}

	public List<TimetableDto> getTimetables() {
		return timetables;
	}

	public void setTimetables(List<TimetableDto> timetables) {
		this.timetables = timetables;
	}

	public List<UiTimetableBean> getTrainList() {
		return trainList;
	}

	public List<StationDto> getStationList() {
		return stationList;
	}

	public void setStationList(List<StationDto> stationList) {
		this.stationList = stationList;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<String> displayStations(String query) {
		AllStationsResponse stationsResponse = stationService.displayAllStations();
		List<String> results = new ArrayList<>();
		if (stationsResponse.getMessages().isEmpty()) {
			List<StationDto> stationList = (List<StationDto>) stationsResponse.getStations();
			stationList.forEach(station -> {
				if (StringUtils.startsWith(station.getStationName(), query)) {
					results.add(station.getStationName());
				}
			});

		}
		return results;
	}

	public String viewStationTimetable() {
		error = "";
		StationTimetableRequest request = new StationTimetableRequest();
		request.setStation(stationName);
		request.setDate(
				LocalDate.parse(date, DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss zzz uuuu", Locale.ROOT)));
		StationTimetableResponse response = timetableService.getSchedule(request);

		if (response.getMessages().isEmpty()) {
			trainList = LocalDateTimeToStringConverter.convertDateTime((List<TimetableDto>) response.getTrains());
			stationName = "";
			date = "";
			return "infoBoard";
		} else {
			response.getMessages().forEach(message -> {
				if (message.type.equals(MessageType.INFO)) {
					error = message.getText();
				} else {
					error = "Some technical error has occured. Please try again later.";
				}
			});
			trainList = Collections.emptyList();
			return "infoBoard";
		}
	}
}
