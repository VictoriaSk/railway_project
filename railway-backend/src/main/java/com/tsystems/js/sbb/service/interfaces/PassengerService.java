package com.tsystems.js.sbb.service.interfaces;

import com.tsystems.js.sbb.service.dto.ClientSearchRequest;
import com.tsystems.js.sbb.service.dto.ClientSearchResponse;

public interface PassengerService {

	ClientSearchResponse searchPassengers(ClientSearchRequest request);

}