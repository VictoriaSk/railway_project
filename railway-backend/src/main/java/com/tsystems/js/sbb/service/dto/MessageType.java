package com.tsystems.js.sbb.service.dto;

public enum MessageType {
    TECH_ERROR,
    ERROR,
    WARN,
    INFO
}

