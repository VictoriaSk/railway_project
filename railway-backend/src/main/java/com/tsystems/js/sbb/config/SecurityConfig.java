package com.tsystems.js.sbb.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().
				antMatchers("/addTrain.xhtml")
				.access("hasRole('ROLE_ADMIN')")
				.antMatchers("/adminStartPage.xhtml")
				.access("hasRole('ROLE_ADMIN')")
				.antMatchers("/addStation.xhtml")
				.access("hasRole('ROLE_ADMIN')")
				.antMatchers("/adminInfoBoard.xhtml")
				.access("hasRole('ROLE_ADMIN')")
				.antMatchers("/searchPassengers.xhtml")
				.access("hasRole('ROLE_ADMIN')")
				.antMatchers("/viewTrains.xhtml")
				.access("hasRole('ROLE_ADMIN')").and().formLogin()
				.loginPage("/loginPage.xhtml")
				.loginProcessingUrl("/appLogin")
				.usernameParameter("app_username")
				.passwordParameter("app_password")
				.defaultSuccessUrl("/adminStartPage.xhtml")
				.and().logout()
				.logoutUrl("/appLogout")
				.logoutSuccessUrl("/startPage.xhtml");
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("admin").password(passwordEncoder.encode("admin")).roles("ADMIN");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}