package com.tsystems.js.sbb.service.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class TrainDto {
	private LocalDate date; // Date of train's departure from its first station
	private String routeNum;
	private String departureStation; 
	private String arrivalStation;
	private LocalDateTime arrivalTime;
	private LocalDateTime departureTime;
	private Integer numberOfTickets; // isn't always capacity. These can be tickets available for purchase. 
	
	public String getRouteNum() {
		return routeNum;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public void setRouteNum(String routeNum) {
		this.routeNum = routeNum;
	}
	public String getDepartureStation() {
		return departureStation;
	}
	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}
	public String getArrivalStation() {
		return arrivalStation;
	}
	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}
	public LocalDateTime getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(LocalDateTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public LocalDateTime getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(LocalDateTime departureTime) {
		this.departureTime = departureTime;
	}
	public Integer getNumberOfTickets() {
		return numberOfTickets;
	}
	public void setNumberOfTickets(Integer numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
	}
}
