package com.tsystems.js.sbb.service.interfaces;

import com.tsystems.js.sbb.service.dto.AllStationsResponse;
import com.tsystems.js.sbb.service.dto.SaveStationRequest;
import com.tsystems.js.sbb.service.dto.SaveStationResponse;

public interface StationService {

	SaveStationResponse saveStation(SaveStationRequest request);

	AllStationsResponse displayAllStations();
}
