package com.tsystems.js.sbb.mo;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.js.sbb.entity.Passenger;
import com.tsystems.js.sbb.entity.Station;
import com.tsystems.js.sbb.entity.Ticket;
import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.exceptions.InvalidTravelDateException;
import com.tsystems.js.sbb.exceptions.TrainsNotFoundException;
import com.tsystems.js.sbb.repository.impl.TrainDaoImpl;
import com.tsystems.js.sbb.repository.interfaces.TrainDao;
import com.tsystems.js.sbb.service.dto.TrainDto;
import com.tsystems.js.sbb.service.dto.TrainSearchRequest;

public class TrainFinderTest {

	private TrainFinder trainFinderMock;
	private TrainDao trainDaoMock;
	private String from;
	private String to;

	@Before
	public void setUp() {
		trainDaoMock = Mockito.mock(TrainDaoImpl.class);
		trainFinderMock = Mockito.mock(TrainFinder.class);
		from = "Place1";
		to = "Place2";
	}

	@Test
	public void testSearchSuccess() {
		TrainSearchRequest travelDetails = new TrainSearchRequest();
		LocalDate travelDate = LocalDate.now();
		travelDetails.setFrom(from);
		travelDetails.setTo(to);
		travelDetails.setTravelDate(travelDate);
		Mockito.when(trainFinderMock.getTrainEntityList(travelDetails)).thenReturn(getTestTrains());
		Mockito.when(trainFinderMock.search(travelDetails)).thenCallRealMethod();
		List<TrainDto> resultList = trainFinderMock.search(travelDetails);
		assertEquals(2, resultList.size());
		assertEquals(from, resultList.get(0).getDepartureStation());
		assertEquals(to, resultList.get(1).getArrivalStation());
		assertEquals(LocalDateTime.of(2020, 5, 23, 8, 00), resultList.get(0).getDepartureTime());
		assertEquals(LocalDateTime.of(2020, 5, 23, 9, 00), resultList.get(1).getDepartureTime());
		assertEquals("03", resultList.get(0).getRouteNum());
		assertEquals("007", resultList.get(1).getRouteNum());
	}

	@Test
	public void testGetTrainEntityListSuccessDepartureToday() {
		TrainSearchRequest travelDetails = new TrainSearchRequest();
		LocalDate travelDate = LocalDate.now();
		travelDetails.setFrom(from);
		travelDetails.setTo(to);
		travelDetails.setTravelDate(travelDate);
		LocalDateTime travelTimeStart = LocalDateTime.now().withSecond(0).withNano(0).plusMinutes(10);
		LocalDateTime travelTimeEnd = travelDate.atTime(23, 59);
		LocalDateTime timeOfArrivalEnd = travelTimeEnd.plusDays(10L);
		Mockito.doReturn(getTestTrains()).when(trainFinderMock).getTrainsByStationNamesAndTravelDate(from, to,
				travelTimeStart, travelTimeEnd, timeOfArrivalEnd);
		Mockito.when(trainFinderMock.getTrainEntityList(travelDetails)).thenCallRealMethod();
		List<Train> resultList = trainFinderMock.getTrainEntityList(travelDetails);
		assertEquals(2, resultList.size());
		assertEquals("03", resultList.get(0).getRouteNum());
		assertEquals("007", resultList.get(1).getRouteNum());
		assertEquals((Integer) 1000, resultList.get(1).getCapacity());
	}

	@Test
	public void testGetTrainEntityListSuccessDepartureLater() {
		TrainSearchRequest travelDetails = new TrainSearchRequest();
		LocalDate travelDate = LocalDate.of(2020, 12, 12);
		travelDetails.setFrom(from);
		travelDetails.setTo(to);
		travelDetails.setTravelDate(travelDate);
		LocalDateTime travelTimeStart = travelDate.atStartOfDay();
		LocalDateTime travelTimeEnd = travelDate.atTime(23, 59);
		LocalDateTime timeOfArrivalEnd = travelDate.atTime(23, 59).plusDays(10L);
		Mockito.doReturn(getTestTrains()).when(trainFinderMock).getTrainsByStationNamesAndTravelDate(from, to,
				travelTimeStart, travelTimeEnd, timeOfArrivalEnd);
		Mockito.when(trainFinderMock.getTrainEntityList(travelDetails)).thenCallRealMethod();
		List<Train> resultList = trainFinderMock.getTrainEntityList(travelDetails);
		assertEquals(2, resultList.size());
		assertEquals("03", resultList.get(0).getRouteNum());
		assertEquals("007", resultList.get(1).getRouteNum());
		assertEquals((Integer) 1000, resultList.get(1).getCapacity());
	}

	@Test
	public void testGetTrainEntityListInvalidTravelDate() {
		TrainSearchRequest travelDetails = new TrainSearchRequest();
		LocalDate travelDate = LocalDate.of(1900, 12, 12);
		travelDetails.setFrom(from);
		travelDetails.setTo(to);
		travelDetails.setTravelDate(travelDate);
		Mockito.when(trainFinderMock.getTrainEntityList(travelDetails)).thenCallRealMethod();
		try {
			trainFinderMock.getTrainEntityList(travelDetails);
		} catch (InvalidTravelDateException ex) {
			// test passed
		}
	}

	@Test
	public void testGetTrainEntityListTrainsNotFound() {
		TrainSearchRequest travelDetails = new TrainSearchRequest();
		LocalDate travelDate = LocalDate.of(2020, 12, 12);
		travelDetails.setFrom(from);
		travelDetails.setTo(to);
		travelDetails.setTravelDate(travelDate);
		LocalDateTime travelTimeStart = travelDate.atStartOfDay();
		LocalDateTime travelTimeEnd = travelDate.atTime(23, 59);
		LocalDateTime timeOfArrivalEnd = travelDate.atTime(23, 59).plusDays(10L);
		Mockito.doReturn(Collections.emptyList()).when(trainFinderMock).getTrainsByStationNamesAndTravelDate(from, to,
				travelTimeStart, travelTimeEnd, timeOfArrivalEnd);
		Mockito.when(trainFinderMock.getTrainEntityList(travelDetails)).thenCallRealMethod();
		try {
			trainFinderMock.getTrainEntityList(travelDetails);
		} catch (TrainsNotFoundException ex) {
			// test passed
		}
	}

	@Test
	public void testGetTrainsByStationNamesAndTravelDateSuccess() {
		TrainFinder sut = new TrainFinder(trainDaoMock);
		LocalDate travelDate = LocalDate.of(2020, 12, 01);
		LocalDateTime travelTimeStart = travelDate.atStartOfDay();
		LocalDateTime travelTimeEnd = travelDate.atTime(23, 59);
		LocalDateTime timeOfArrivalEnd = travelDate.atTime(23, 59).plusDays(10L);
		Mockito.doReturn(getTestDepartureTimetables()).when(trainDaoMock)
				.getTimetablesByDepartureStationNameAndDepartureDate(from, travelTimeStart, travelTimeEnd);
		Mockito.doReturn(getTestArrivalTimetables()).when(trainDaoMock)
				.getTimetablesByArrivalStationNameAndArrivalPeriod(to, travelTimeStart, timeOfArrivalEnd);
		Timetable departureTimetable = getTestDepartureTimetables().get(0);
		Train train = departureTimetable.getTrain();
		Mockito.when(trainDaoMock.getById(departureTimetable.getTrain().getTrainId())).thenReturn(train);
		List<Train> resultList = sut.getTrainsByStationNamesAndTravelDate(from, to, travelTimeStart, travelTimeEnd,
				timeOfArrivalEnd);
		assertEquals(1, resultList.size());
		assertEquals("03", resultList.get(0).getRouteNum());
	}

	@Test
	public void testGetTrainsByStationNamesAndTravelDateNoTickets() {
		TrainFinder sut = new TrainFinder(trainDaoMock);
		LocalDate travelDate = LocalDate.of(2020, 12, 01);
		LocalDateTime travelTimeStart = travelDate.atStartOfDay();
		LocalDateTime travelTimeEnd = travelDate.atTime(23, 59);
		LocalDateTime timeOfArrivalEnd = travelDate.atTime(23, 59).plusDays(10L);
		Mockito.doReturn(getTestDepartureTimetables()).when(trainDaoMock)
				.getTimetablesByDepartureStationNameAndDepartureDate(from, travelTimeStart, travelTimeEnd);
		Mockito.doReturn(getTestArrivalTimetables()).when(trainDaoMock)
				.getTimetablesByArrivalStationNameAndArrivalPeriod(to, travelTimeStart, timeOfArrivalEnd);
		Timetable departureTimetable = getTestDepartureTimetables().get(0);
		Train ghostTrain = new Train();
		ghostTrain.setCapacity(0);
		Mockito.when(trainDaoMock.getById(departureTimetable.getTrain().getTrainId())).thenReturn(ghostTrain);
		List<Train> resultList = sut.getTrainsByStationNamesAndTravelDate(from, to, travelTimeStart, travelTimeEnd,
				timeOfArrivalEnd);
		assertEquals(0, resultList.size());
	}

	private List<Train> getTestTrains() {
		Train firstTrain = new Train("03", 1000);
		Timetable firstDeparture = new Timetable();
		Station firstDepartureStation = new Station();
		firstDepartureStation.setStationName("Place1");
		firstDeparture.setStation(firstDepartureStation);
		firstDeparture.setDepartureTime(LocalDateTime.of(2020, 5, 23, 8, 00));

		Timetable firstArrival = new Timetable();
		Station firstArrivalStation = new Station();
		firstArrivalStation.setStationName("Place2");
		firstArrival.setStation(firstArrivalStation);
		firstArrival.setArrivalTime(LocalDateTime.of(2020, 5, 23, 10, 00));

		Set<Timetable> firstTrainTimetables = new HashSet<Timetable>();
		firstTrainTimetables.add(firstDeparture);
		firstTrainTimetables.add(firstArrival);
		firstTrain.setTrainTimetable(firstTrainTimetables);

		Ticket firstTrainTicket = new Ticket(firstTrain, new Passenger());
		Set<Ticket> firstTrainTickets = new HashSet<Ticket>();
		firstTrainTickets.add(firstTrainTicket);

		Train secondTrain = new Train("007", 1000);
		Timetable secondDeparture = new Timetable();
		Station secondDepartureStation = new Station();
		secondDepartureStation.setStationName("Place1");
		secondDeparture.setStation(secondDepartureStation);
		secondDeparture.setDepartureTime(LocalDateTime.of(2020, 5, 23, 9, 00));

		Timetable secondArrival = new Timetable();
		Station secondArrivalStation = new Station();
		secondArrivalStation.setStationName("Place2");
		secondArrival.setStation(secondArrivalStation);
		secondArrival.setArrivalTime(LocalDateTime.of(2020, 5, 23, 12, 00));

		Set<Timetable> secondTrainTimetables = new HashSet<Timetable>();
		secondTrainTimetables.add(secondDeparture);
		secondTrainTimetables.add(secondArrival);
		secondTrain.setTrainTimetable(secondTrainTimetables);

		return List.of(firstTrain, secondTrain);
	}

	private List<Timetable> getTestDepartureTimetables() {
		Train train = new Train("03", 1000);
		train.setTrainId(123L);
		Ticket ticket = new Ticket(train, new Passenger());
		Set<Ticket> tickets = new HashSet<Ticket>();
		tickets.add(ticket);
		Timetable departure = new Timetable();
		departure.setTrain(train);
		return List.of(departure);
	}

	private List<Timetable> getTestArrivalTimetables() {
		Train train = new Train("03", 1000);
		train.setTrainId(123L);
		Ticket ticket = new Ticket(train, new Passenger());
		Set<Ticket> tickets = new HashSet<Ticket>();
		tickets.add(ticket);
		Timetable arrival = new Timetable();
		arrival.setTrain(train);
		return List.of(arrival);
	}
}
