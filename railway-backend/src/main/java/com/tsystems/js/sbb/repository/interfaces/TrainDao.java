package com.tsystems.js.sbb.repository.interfaces;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.entity.Train;

public interface TrainDao {

	List<Train> getAll();

	List<Timetable> getTimetablesByDepartureStationNameAndDepartureDate(String departureStation,
			LocalDateTime timeOfDepartureStart, LocalDateTime timeOfDepartureEnd);

	Train getById(Long trainId);

	void save(Train train);

	List<Timetable> getTimetablesByArrivalStationNameAndArrivalPeriod(String arrivalStation,
			LocalDateTime timeOfDepartureStart, LocalDateTime timeOfArrivalEnd);

	List<Train> getTrainsByRouteNumberAndDepartureDate(String routeNum, LocalDate departureDate);

}
