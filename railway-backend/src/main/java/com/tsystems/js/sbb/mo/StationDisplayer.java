package com.tsystems.js.sbb.mo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.js.sbb.entity.Station;
import com.tsystems.js.sbb.repository.interfaces.StationDao;
import com.tsystems.js.sbb.service.dto.StationDto;
import com.tsystems.js.sbb.service.dto.StationMapper;

@Component
public class StationDisplayer {

	private final StationDao stationDao;

	public StationDisplayer(StationDao stationDao) {
		this.stationDao = stationDao;
	}

	@Transactional
	public List<StationDto> getAll() {
		List<StationDto> resultList = new ArrayList<>();
		List<Station> allStations = stationDao.getAll();
		allStations.forEach(station -> resultList.add(StationMapper.INSTANCE.toDto(station)));
		return sort(resultList);
	}

	private List<StationDto> sort(List<StationDto> listToSort) {
		List<StationDto> sortedList = listToSort.stream().sorted((a, b) -> {
			return a.getStationName().compareTo(b.getStationName());
		}).collect(Collectors.toList());
		return sortedList;
	}

}
