package com.tsystems.js.sbb.exceptions;

import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;

public class InvalidTravelDateException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private Message message = new Message ("Input data is invalid. Please make sure the date you entered is correct.", MessageType.INFO);

	public String getMessage() {
		return message.getText();
	}
}
