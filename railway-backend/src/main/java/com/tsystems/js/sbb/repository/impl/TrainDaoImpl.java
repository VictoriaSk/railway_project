package com.tsystems.js.sbb.repository.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.repository.interfaces.TrainDao;

@Repository
public class TrainDaoImpl implements TrainDao {

	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<Train> getAll() {
		@SuppressWarnings("unchecked")
		TypedQuery<Train> query = sessionFactory.getCurrentSession().createQuery("from Train");
		return query.getResultList();
	}

	@Override
	public Train getById(Long trainId) {
		@SuppressWarnings("unchecked")
		TypedQuery<Train> query = sessionFactory.getCurrentSession().createQuery("from Train where trainId = :trainId");
		query.setParameter("trainId", trainId);
		return query.getSingleResult();
	}
	

	@Override
	public List<Timetable> getTimetablesByDepartureStationNameAndDepartureDate(String departureStation,
			LocalDateTime timeOfDepartureStart, LocalDateTime timeOfDepartureEnd) {
		@SuppressWarnings("unchecked")
		TypedQuery<Timetable> query = sessionFactory.getCurrentSession().createQuery(
				"select t from Timetable t inner join t.station st where st.stationName = :departureStation and t.departureTime between :time1 and :time2");
		query.setParameter("departureStation", departureStation);
		query.setParameter("time1", timeOfDepartureStart);
		query.setParameter("time2", timeOfDepartureEnd);
		return query.getResultList();
	}
	

	@Override
	public List<Timetable> getTimetablesByArrivalStationNameAndArrivalPeriod(String arrivalStation, LocalDateTime timeOfArrivalStart, LocalDateTime timeOfArrivalEnd) {
		@SuppressWarnings("unchecked")
		TypedQuery<Timetable> query = sessionFactory.getCurrentSession().createQuery(
				"select t from Timetable t inner join t.station st where st.stationName = :arrivalStation and t.arrivalTime between :time1 and :time2");
		query.setParameter("arrivalStation", arrivalStation);
		query.setParameter("time1", timeOfArrivalStart);
		query.setParameter("time2", timeOfArrivalEnd);
		return query.getResultList();
	}

	@Override
	public List <Train> getTrainsByRouteNumberAndDepartureDate (String routeNum, 
			LocalDate departureDate) {
		@SuppressWarnings("unchecked")
		TypedQuery<Train> query = sessionFactory.getCurrentSession().createQuery(
				"select tr from Train tr inner join tr.trainTimetable t where tr.routeNum = :routeNum and t.departureTime between :time1 and :time2");
		query.setParameter("routeNum", routeNum);
		query.setParameter("time1", departureDate.atStartOfDay());
		query.setParameter("time2", departureDate.atTime(23, 59));
		return query.getResultList();
	}
	
	@Override
	public void save(Train train) {
		sessionFactory.getCurrentSession().save(train);
	}

	
}
