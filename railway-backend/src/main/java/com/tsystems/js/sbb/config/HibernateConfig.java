package com.tsystems.js.sbb.config;

import com.tsystems.js.sbb.entity.Passenger;
import com.tsystems.js.sbb.entity.Station;
import com.tsystems.js.sbb.entity.Ticket;
import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.entity.Train;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScans(value = {@ComponentScan("com.tsystems.js.sbb")})
public class HibernateConfig {

    private ApplicationContext context;

    @Bean
    public LocalSessionFactoryBean getSessionFactory() {
        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
        factoryBean.setConfigLocation(context.getResource("classpath:hibernate.cfg.xml"));
        factoryBean.setAnnotatedClasses(Station.class, Train.class, Passenger.class, Ticket.class, Timetable.class);
        return factoryBean;
    }

    @Bean
    public HibernateTransactionManager getTransactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(getSessionFactory().getObject());
        return transactionManager;
    }

    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }
}
