package com.tsystems.js.sbb.service.dto;

public class SaveStationRequest {
    private StationDto stationDto;

   	public SaveStationRequest(StationDto stationDto) {
		this.stationDto = stationDto;
	}

	public StationDto getStationDto() {
		return stationDto;
	}

	public void setStationDto(StationDto stationDto) {
		this.stationDto = stationDto;
	}
}
