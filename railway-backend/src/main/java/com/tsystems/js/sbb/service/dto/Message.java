package com.tsystems.js.sbb.service.dto;

public class Message {
    private String text;
    public MessageType type;

    public Message(String text, MessageType type) {
        super();
        this.text = text;
        this.type = type;
    }

    public String getText() {
        return text;
    }

}