package com.tsystems.js.sbb.mo;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.js.sbb.entity.Station;
import com.tsystems.js.sbb.exceptions.StationExistsException;
import com.tsystems.js.sbb.repository.interfaces.StationDao;
import com.tsystems.js.sbb.service.dto.SaveStationRequest;
import com.tsystems.js.sbb.service.dto.StationDto;
import com.tsystems.js.sbb.service.dto.StationMapper;

@Component
public class StationSaver {

	private final StationDao stationDao;

	public StationSaver(StationDao stationDao) {
		this.stationDao = stationDao;
	}

	@Transactional
	public StationDto save(SaveStationRequest request) throws StationExistsException {
		List<Station> entities = stationDao.getAll();
		StationDto stationDto = request.getStationDto();
		entities.forEach(entity -> {
			if (entity.getStationName().equals(stationDto.getStationName())) {
				throw new StationExistsException();
			}
		});
		stationDao.save(StationMapper.INSTANCE.toEntity(stationDto));
		return stationDto;
	}
}
