package com.tsystems.js.sbb.service.dto;

import java.util.Collection;

public class TrainStopAddResponse {
	private TrainDto train;
	private Collection<Message> messages;

	public TrainStopAddResponse(TrainDto train, Collection<Message> messages) {
		this.train = train;
		this.messages = messages;
	}
	
	public TrainStopAddResponse(Collection<Message> messages) {
		this.messages = messages;
	}

	public TrainDto getTrain() {
		return train;
	}

	public void setTrain(TrainDto train) {
		this.train = train;
	}

	public Collection<Message> getMessages() {
		return messages;
	}

	public void setMessages(Collection<Message> messages) {
		this.messages = messages;
	}
}
