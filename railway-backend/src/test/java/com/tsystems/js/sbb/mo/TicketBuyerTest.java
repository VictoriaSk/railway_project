package com.tsystems.js.sbb.mo;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.js.sbb.entity.Passenger;
import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.exceptions.AlreadyHasTicketException;
import com.tsystems.js.sbb.repository.impl.PassengerDaoImpl;
import com.tsystems.js.sbb.repository.impl.TicketDaoImpl;
import com.tsystems.js.sbb.repository.impl.TrainDaoImpl;
import com.tsystems.js.sbb.repository.interfaces.PassengerDao;
import com.tsystems.js.sbb.repository.interfaces.TicketDao;
import com.tsystems.js.sbb.repository.interfaces.TrainDao;
import com.tsystems.js.sbb.service.dto.BuyTicketRequest;
import com.tsystems.js.sbb.service.dto.TicketDto;

public class TicketBuyerTest {
	private TicketBuyer sut;
	private TicketDao ticketDaoMock;
	private TrainDao trainDaoMock;
	private PassengerDao passengerDaoMock;
	private BuyTicketRequest request;
	private String trainNumber;
	private LocalDate departureDate;
	private String name;
	private String surname;
	private LocalDate dateOfBirth;

	@Before
	public void setUp() {
		ticketDaoMock = Mockito.mock(TicketDaoImpl.class);
		trainDaoMock = Mockito.mock(TrainDaoImpl.class);
		passengerDaoMock = Mockito.mock(PassengerDaoImpl.class);
		sut = new TicketBuyer(ticketDaoMock, trainDaoMock, passengerDaoMock);
		request = new BuyTicketRequest();
		request.setDateOfBirth(LocalDate.now());
		request.setName("Name");
		request.setSurname("Surname");
		request.setDepartureDate(LocalDate.now());
		request.setTrainNumber("007");
		name = "Name";
		surname = "Surname";
		dateOfBirth = LocalDate.now();
		departureDate = LocalDate.now();
		trainNumber = "007";
	}

	@Test
	public void testBuySuccessSomePassengers() {
		List<Train> chosenTrain = List.of(new Train("007", 500));
		Mockito.when(trainDaoMock.getTrainsByRouteNumberAndDepartureDate(trainNumber, departureDate))
				.thenReturn(chosenTrain);
		Passenger firstPassenger = new Passenger("First", "First", LocalDate.now());
		Passenger secondPassenger = new Passenger("Second", "Second", LocalDate.now());
		List<Passenger> passengersOfChosenTrain = List.of(firstPassenger, secondPassenger);
		Mockito.when(passengerDaoMock.getPassengersByTrainId(chosenTrain.get(0).getTrainId()))
				.thenReturn(passengersOfChosenTrain);
		Passenger passenger = new Passenger(name, surname, dateOfBirth);
		Mockito.when(passengerDaoMock.getPassenger(name, surname, dateOfBirth)).thenReturn(null);
		Mockito.doNothing().when(passengerDaoMock).save(passenger);
		TicketDto resultTicket = sut.buy(request);
		assertEquals(trainNumber, resultTicket.getRouteNum());
		assertEquals(departureDate, resultTicket.getDepartureDate());
		assertEquals(surname, resultTicket.getSurname());
	}

	@Test
	public void testBuySuccessNoPassengers() {
		List<Train> chosenTrain = List.of(new Train("007", 500));
		Passenger passenger = new Passenger(name, surname, dateOfBirth);
		Mockito.when(trainDaoMock.getTrainsByRouteNumberAndDepartureDate(trainNumber, departureDate))
				.thenReturn(chosenTrain);
		Mockito.when(passengerDaoMock.getPassengersByTrainId(chosenTrain.get(0).getTrainId())).thenReturn(List.of());
		Mockito.when(passengerDaoMock.getPassenger(name, surname, dateOfBirth)).thenReturn(null);
		Mockito.doNothing().when(passengerDaoMock).save(passenger);
		TicketDto resultTicket = sut.buy(request);
		assertEquals(trainNumber, resultTicket.getRouteNum());
		assertEquals(departureDate, resultTicket.getDepartureDate());
		assertEquals(surname, resultTicket.getSurname());
	}

	@Test
	public void testBuyAlreadyHasTickets() {
		List<Train> chosenTrain = List.of(new Train("007", 500));
		Mockito.when(trainDaoMock.getTrainsByRouteNumberAndDepartureDate(trainNumber, departureDate))
				.thenReturn(chosenTrain);
		Passenger firstPassenger = new Passenger("First", "First", LocalDate.now());
		Passenger secondPassenger = new Passenger("Second", "Second", LocalDate.now());
		Passenger nextPassenger = new Passenger("Name", "Surname", LocalDate.now());
		Mockito.when(passengerDaoMock.getPassengersByTrainId(chosenTrain.get(0).getTrainId()))
				.thenReturn(List.of(firstPassenger, secondPassenger, nextPassenger));
		try {
			sut.buy(request);
		} catch (AlreadyHasTicketException ex) {
		}
	}

	@Test
	public void testGenerateAndSavePassengerExists() {
		Passenger existingPassenger = new Passenger(name, surname, dateOfBirth);
		Mockito.when(passengerDaoMock.getPassenger(name, surname, dateOfBirth)).thenReturn(existingPassenger);
		Passenger resultPassenger = sut.generateAndSavePassenger(name, surname, dateOfBirth);
		assertEquals(name, resultPassenger.getName());
		assertEquals(dateOfBirth, resultPassenger.getDateOfBirth());
	}

	@Test
	public void testGenerateAndSavePassengerDoesntExist() {
		Passenger passenger = new Passenger(name, surname, dateOfBirth);
		Mockito.when(passengerDaoMock.getPassenger(name, surname, dateOfBirth)).thenReturn(null);
		Mockito.doNothing().when(passengerDaoMock).save(passenger);
		Passenger resultPassenger = sut.generateAndSavePassenger(name, surname, dateOfBirth);
		assertEquals(name, resultPassenger.getName());
		assertEquals(dateOfBirth, resultPassenger.getDateOfBirth());
	}
}
