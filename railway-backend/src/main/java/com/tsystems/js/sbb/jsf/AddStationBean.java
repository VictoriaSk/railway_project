package com.tsystems.js.sbb.jsf;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.tsystems.js.sbb.service.dto.AllStationsResponse;
import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;
import com.tsystems.js.sbb.service.dto.SaveStationRequest;
import com.tsystems.js.sbb.service.dto.SaveStationResponse;
import com.tsystems.js.sbb.service.dto.StationDto;
import com.tsystems.js.sbb.service.interfaces.StationService;

@Component
@RequestScoped
@ManagedBean
public class AddStationBean {

	private StationService stationService;

	@ManagedProperty("#{param.stName}")
	private String stName;
	
	private List<StationDto> stationList;
	
	private String error;
	
	private boolean flag;
	
	public AddStationBean() {
	}

	@Autowired
	public void setStationService(StationService stationService) {
		this.stationService = stationService;
	}

	public String getStName() {
		return stName;
	}

	public void setStName(String stName) {
		this.stName = stName;
	}

	public String getError() {
		return error;
	}
	
	public List<StationDto> getStationList() {
		return stationList;
	}

	public void setStationList(List<StationDto> stationList) {
		this.stationList = stationList;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public List<StationDto> getStations() {
		AllStationsResponse stationsResponse = stationService.displayAllStations();
		if (stationsResponse.getMessages().isEmpty()) {
			stationList = (List<StationDto>) stationsResponse.getStations();
		}
		return stationList;
	}

	public String saveStation() {
		error="";
		flag=false;
		StationDto newStation = new StationDto();
		newStation.setStationName(stName);
		SaveStationRequest request = new SaveStationRequest(newStation);
		SaveStationResponse response = stationService.saveStation(request);

		if (response.getMessages().isEmpty()) {
			stName="";
			flag=true;
			return "addStation";

		} else {
			flag = false;
			for (Message message : response.getMessages()) {
				if (message.type.equals(MessageType.INFO)) {
					error = message.getText();
				} else {
					error = "Some technical error has occured. Please try again later.";
				}
			}
			return "addStation";
		}
	}
}
