package com.tsystems.js.sbb.mo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.repository.interfaces.TrainDao;
import com.tsystems.js.sbb.service.dto.TrainDto;

@Component
public class TrainDisplayer {

	private final TrainDao trainDao;

	public TrainDisplayer(TrainDao trainDao) {
		this.trainDao = trainDao;
	}

	@Transactional
	public List<TrainDto> getAll() {
		List<TrainDto> resultList = new ArrayList<TrainDto>();
		List<Train> allTrains = trainDao.getAll();
			allTrains.forEach(train -> {
			TrainDto trainDto = new TrainDto();
			trainDto.setRouteNum(train.getRouteNum());
			train.getTrainTimetable().forEach(timetable -> {
				if (timetable.getArrivalTime() == null) {
					trainDto.setDepartureTime(timetable.getDepartureTime());
					trainDto.setDepartureStation(timetable.getStation().getStationName());
				}
				if (timetable.getDepartureTime() == null) {
					trainDto.setArrivalTime(timetable.getArrivalTime());
					trainDto.setArrivalStation(timetable.getStation().getStationName());
				}
			});
			resultList.add(trainDto);
		});
		return sortByDepartureTime(resultList);
	}

	private List<TrainDto> sortByDepartureTime(List<TrainDto> listToSort) {
		List<TrainDto> sortedList = listToSort.stream().sorted((a, b) -> {
			return a.getDepartureTime().compareTo(b.getDepartureTime());
		}).collect(Collectors.toList());
		return sortedList;
	}
}
