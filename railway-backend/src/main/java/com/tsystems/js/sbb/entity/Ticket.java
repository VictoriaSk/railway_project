package com.tsystems.js.sbb.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "ticket")
@Entity
public class Ticket {

	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ticket_id")
	private Long ticketId;

	public Ticket(Train train, Passenger passenger) {
		super();
		this.train = train;
		this.passenger = passenger;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "train")
	private Train train;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "passenger")
	private Passenger passenger;

	public Ticket() {

	}

	public void setId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public Long getId() {
		return this.ticketId;
	}

	public void setTrain(Train train) {
		this.train = train;
	}

	public Train getTrain() {
		return this.train;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

	public Passenger getPassenger() {
		return this.passenger;
	}

}
