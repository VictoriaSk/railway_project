package com.tsystems.js.sbb.mo;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.js.sbb.entity.Station;
import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.repository.impl.TrainDaoImpl;
import com.tsystems.js.sbb.repository.interfaces.TrainDao;
import com.tsystems.js.sbb.service.dto.TrainDto;

public class TrainDisplayerTest {

	private TrainDisplayer sut;
	private TrainDao trainDaoMock;

	@Before
	public void setUp() {
		trainDaoMock = Mockito.mock(TrainDaoImpl.class);
		sut = new TrainDisplayer(trainDaoMock);
	}

	@Test
	public void testGetAllSuccess() {
		Mockito.when(trainDaoMock.getAll()).thenReturn(getTestTrains());
		List<TrainDto> resultList = sut.getAll();
		assertEquals(2, resultList.size());
		assertEquals("FirstDepStation", resultList.get(0).getDepartureStation());
		assertEquals("SecondArrStation", resultList.get(1).getArrivalStation());
		assertEquals(LocalDateTime.of(2020, 5, 23, 8, 00), resultList.get(0).getDepartureTime());
		assertEquals(LocalDateTime.of(2020, 5, 23, 9, 00), resultList.get(1).getDepartureTime());
		assertEquals("001", resultList.get(0).getRouteNum());
		assertEquals("002", resultList.get(1).getRouteNum());
	}

	private List<Train> getTestTrains() {
		Train firstTrain = new Train("001", 100);
		Timetable firstDeparture = new Timetable();
		Station firstDepartureStation = new Station();
		firstDepartureStation.setStationName("FirstDepStation");
		firstDeparture.setStation(firstDepartureStation);
		firstDeparture.setDepartureTime(LocalDateTime.of(2020, 5, 23, 8, 00));

		Timetable firstArrival = new Timetable();
		Station firstArrivalStation = new Station();
		firstArrivalStation.setStationName("FirstArrStation");
		firstArrival.setStation(firstArrivalStation);
		firstArrival.setArrivalTime(LocalDateTime.of(2020, 5, 23, 10, 00));

		Set<Timetable> firstTrainTimetables = new HashSet<Timetable>();
		firstTrainTimetables.add(firstDeparture);
		firstTrainTimetables.add(firstArrival);
		firstTrain.setTrainTimetable(firstTrainTimetables);

		Train secondTrain = new Train("002", 100);
		Timetable secondDeparture = new Timetable();
		Station secondDepartureStation = new Station();
		secondDepartureStation.setStationName("SecondDepStation");
		secondDeparture.setStation(secondDepartureStation);
		secondDeparture.setDepartureTime(LocalDateTime.of(2020, 5, 23, 9, 00));

		Timetable secondArrival = new Timetable();
		Station secondArrivalStation = new Station();
		secondArrivalStation.setStationName("SecondArrStation");
		secondArrival.setStation(secondArrivalStation);
		secondArrival.setArrivalTime(LocalDateTime.of(2020, 5, 23, 12, 00));

		Set<Timetable> secondTrainTimetables = new HashSet<Timetable>();
		secondTrainTimetables.add(secondDeparture);
		secondTrainTimetables.add(secondArrival);
		secondTrain.setTrainTimetable(secondTrainTimetables);

		return List.of(firstTrain, secondTrain);
	}
}
