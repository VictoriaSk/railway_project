package com.tsystems.js.sbb.mo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.js.sbb.entity.Passenger;
import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.exceptions.PassengersNotFoundException;
import com.tsystems.js.sbb.exceptions.TrainMissingException;
import com.tsystems.js.sbb.repository.interfaces.PassengerDao;
import com.tsystems.js.sbb.repository.interfaces.TrainDao;
import com.tsystems.js.sbb.service.dto.ClientSearchRequest;
import com.tsystems.js.sbb.service.dto.PassengerDto;
import com.tsystems.js.sbb.service.dto.PassengerMapper;

@Component
public class PassengerFinder {

	private final PassengerDao passengerDao;
	private final TrainDao trainDao;
	
	public PassengerFinder(PassengerDao passengerDao, TrainDao trainDao) {
		this.passengerDao = passengerDao;
		this.trainDao = trainDao;
	}

	@Transactional 
	public Collection<PassengerDto> search(ClientSearchRequest request)
			throws TrainMissingException, PassengersNotFoundException {
		LocalDate departureDate = request.getDepartureDate();
		String trainNumber = request.getTrainNumber();

		List<Train> chosenTrain = trainDao.getTrainsByRouteNumberAndDepartureDate(trainNumber, departureDate);
		if (chosenTrain.isEmpty()) {
			throw new TrainMissingException();
		} else {
			List<Passenger> passengers = passengerDao.getPassengersByTrainId(chosenTrain.get(0).getTrainId());
			if (passengers.isEmpty()) {
				throw new PassengersNotFoundException();
			} else {
				List<PassengerDto> result = new ArrayList<>();
				passengers.forEach(passenger -> result.add(PassengerMapper.INSTANCE.toDto(passenger)));
				return sortBySurname(result);
			}
		}
	}
	
	private List<PassengerDto> sortBySurname(List<PassengerDto> listToSort) {
		List<PassengerDto> sortedList = listToSort.stream().sorted((a, b) -> {
			return a.getSurname().compareTo(b.getSurname());
		}).collect(Collectors.toList());
		return sortedList;
	}
}

