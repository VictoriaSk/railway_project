package com.tsystems.js.sbb.service.dto;

import java.time.LocalDate;

public class ClientSearchRequest {
  
    private LocalDate departureDate; 
    private String trainNumber;


    public LocalDate getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(LocalDate departureDate) {
		this.departureDate = departureDate;
	}

	public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }
}
