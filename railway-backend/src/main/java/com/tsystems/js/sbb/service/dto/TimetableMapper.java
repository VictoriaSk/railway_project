package com.tsystems.js.sbb.service.dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.tsystems.js.sbb.entity.Timetable;

@Mapper
public interface TimetableMapper {

	TimetableMapper INSTANCE = Mappers.getMapper(TimetableMapper.class);

	@Mapping(source = "timetable.arrivalTime", target = "arrivalTime")
	@Mapping(source = "timetable.departureTime", target = "departureTime")
	@Mapping(source = "timetable.train.routeNum", target = "routeNum")
	@Mapping(source = "departureStation", target = "departureStationName")
	@Mapping(source = "arrivalStation", target = "arrivalStationName")
	TimetableDto toDto(Timetable timetable, String departureStation, String arrivalStation);

}
