package com.tsystems.js.sbb.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "station")
@Entity
public class Station {

	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "station_id")
	private Long stationId;

	@OneToMany(mappedBy = "station", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Timetable> stationTimetable = new HashSet<Timetable>();

	@Column(name = "station_name")
	private String stationName;

	public Station() {

	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Set<Timetable> getStationTimetable() {
		return stationTimetable;
	}

	public void setStationTimetable(Set<Timetable> stationTimetable) {
		this.stationTimetable = stationTimetable;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
}
	