package com.tsystems.js.sbb.mo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.exceptions.NoTrainsPassingException;
import com.tsystems.js.sbb.repository.interfaces.TimetableDao;
import com.tsystems.js.sbb.service.dto.LiveTimetableRequest;
import com.tsystems.js.sbb.service.dto.StationTimetableRequest;
import com.tsystems.js.sbb.service.dto.TimetableDto;
import com.tsystems.js.sbb.service.dto.TimetableMapper;

@Component
public class TimetableFormer {

	private final TimetableDao timetableDao;

	public TimetableFormer(TimetableDao timetableDao) {
		this.timetableDao = timetableDao;
	}

	@Transactional
	public Collection<TimetableDto> search(StationTimetableRequest scheduleInfo) throws NoTrainsPassingException {
		String station = scheduleInfo.getStation();
		LocalDate date = scheduleInfo.getDate();
		List<Timetable> stationTimetable = timetableDao.getTimetablesByStationNameAndDate(station, date);
		List<TimetableDto> result = new ArrayList<TimetableDto>();
		stationTimetable.forEach(timetable -> {
			Set<Timetable> currentTrainTimetables = timetable.getTrain().getTrainTimetable();
			String departureStation = StringUtils.EMPTY;
			String arrivalStation = StringUtils.EMPTY;
			for (Timetable trainTimetable : currentTrainTimetables) {
				if (trainTimetable.getArrivalTime() == null) {
					departureStation = trainTimetable.getStation().getStationName();
				}
				if (trainTimetable.getDepartureTime() == null) {
					arrivalStation = trainTimetable.getStation().getStationName();
				}
			}
			result.add(TimetableMapper.INSTANCE.toDto(timetable, departureStation, arrivalStation));
		});
		if (result.isEmpty()) {
			throw new NoTrainsPassingException();
		}
		return sort(result);
	}

	@Transactional
	public Collection<TimetableDto> liveSearch(LiveTimetableRequest scheduleInfo) throws NoTrainsPassingException {
		String station = scheduleInfo.getStation();
		List<Timetable> stationTimetable = timetableDao.getTimetablesByStationNameAndCurrentTime(station);
		List<TimetableDto> result = new ArrayList<TimetableDto>();
		stationTimetable.forEach(timetable -> {
			Set<Timetable> currentTrainTimetables = timetable.getTrain().getTrainTimetable();
			String departureStation = StringUtils.EMPTY;
			String arrivalStation = StringUtils.EMPTY;
			for (Timetable trainTimetable : currentTrainTimetables) {
				if (trainTimetable.getArrivalTime() == null) {
					departureStation = trainTimetable.getStation().getStationName();
				}
				if (trainTimetable.getDepartureTime() == null) {
					arrivalStation = trainTimetable.getStation().getStationName();
				}
			}
			result.add(TimetableMapper.INSTANCE.toDto(timetable, departureStation, arrivalStation));
		});
		if (result.isEmpty()) {
			throw new NoTrainsPassingException();
		}
		return sort(result);
	}

	private List<TimetableDto> sort(List<TimetableDto> listToSort) {
		Comparator<TimetableDto> comparator = new TimetableComparator();
		List<TimetableDto> sortedList = listToSort.stream().sorted(comparator).collect(Collectors.toList());
		return sortedList;
	}
}