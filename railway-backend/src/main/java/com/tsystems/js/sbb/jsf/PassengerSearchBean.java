package com.tsystems.js.sbb.jsf;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tsystems.js.sbb.service.dto.ClientSearchRequest;
import com.tsystems.js.sbb.service.dto.ClientSearchResponse;
import com.tsystems.js.sbb.service.dto.MessageType;
import com.tsystems.js.sbb.service.dto.PassengerDto;
import com.tsystems.js.sbb.service.interfaces.PassengerService;

@Component
@RequestScoped
@ManagedBean
public class PassengerSearchBean {

	private PassengerService passengerService;

	@ManagedProperty("#{param.routeNum}")
	private String trainNumber;

	@ManagedProperty("#{param.depDate}")
	private String departureDate;

	private List<PassengerDto> passengerList;

	private String error;

	public PassengerSearchBean() {
	}

	@Autowired
	public void setPassengerService(PassengerService passengerService) {
		this.passengerService = passengerService;
	}
	
	public String getTrainNumber() {
		return trainNumber;
	}

	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public List<PassengerDto> getPassengerList() {
		return passengerList;
	}

	public void setPassengerList(List<PassengerDto> passengerList) {
		this.passengerList = passengerList;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getTrainPassengers() {
		error = "";
		ClientSearchRequest request = new ClientSearchRequest();
		request.setDepartureDate(LocalDate.parse(departureDate,
				DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss zzz uuuu", Locale.ROOT)));
		request.setTrainNumber(trainNumber);
		ClientSearchResponse response = passengerService.searchPassengers(request);

		if (response.getMessages().isEmpty()) {
			passengerList = (List<PassengerDto>) response.getPassengers();
			departureDate = "";
			trainNumber = "";
		} else {
			departureDate = "";
			trainNumber = "";
			passengerList = Collections.emptyList();
			response.getMessages().forEach(message -> {
				if (message.type.equals(MessageType.INFO)) {
					error = message.getText();
				} else {
					error = "Some technical error occured. Please try again later.";
				}
			});
		}
		return "searchPassengers";
	}
}
