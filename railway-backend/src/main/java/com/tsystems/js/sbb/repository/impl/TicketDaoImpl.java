package com.tsystems.js.sbb.repository.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tsystems.js.sbb.entity.Ticket;
import com.tsystems.js.sbb.repository.interfaces.TicketDao;

@Repository
public class TicketDaoImpl implements TicketDao {

	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void save(Ticket ticket) {
		sessionFactory.getCurrentSession().save(ticket);
	}
}
