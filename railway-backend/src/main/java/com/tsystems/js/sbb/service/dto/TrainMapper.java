package com.tsystems.js.sbb.service.dto;

import java.time.LocalDate;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.entity.Train;

@Mapper
public interface TrainMapper {

	TrainMapper INSTANCE = Mappers.getMapper(TrainMapper.class);

	@Mapping(source = "train.routeNum", target = "routeNum")
	@Mapping(source = "train.capacity", target = "numberOfTickets")
	@Mapping(source = "timetable.station.stationName", target = "departureStation")
	@Mapping(source = "timetable.departureTime", target = "departureTime")
	@Mapping(source = "timetable.arrivalTime", target = "arrivalTime")
	@Mapping(source = "date", target = "date")
	TrainDto toDto(Train train, Timetable timetable, LocalDate date);

	
}

