package com.tsystems.js.sbb.service.dto;

import java.time.LocalDate;

public class StationTimetableRequest {
    private String station;
    private LocalDate date;
    
	public String getStation() {
		return station;
	}
	public void setStation(String station) {
		this.station = station;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	
}
