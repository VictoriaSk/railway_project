package com.tsystems.js.sbb.service.dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.tsystems.js.sbb.entity.Station;

@Mapper
public interface StationMapper {

	StationMapper INSTANCE = Mappers.getMapper(StationMapper.class);

	@Mapping(source = "stationName", target = "stationName")
	@Mapping(source = "stationTimetable", target = "stationTimetableDto")
	StationDto toDto(Station station);
	
	@Mapping(source = "stationName", target = "stationName")
	@Mapping(source = "stationTimetableDto", target = "stationTimetable")
	Station toEntity(StationDto stationDto);
}
