package com.tsystems.js.sbb.service.dto;

import java.time.LocalDate;

public class TrainSearchRequest {
    private String from;
    private String to;
    private LocalDate travelDate;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public LocalDate getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(LocalDate travelDate) {
        this.travelDate = travelDate;
    }
}
