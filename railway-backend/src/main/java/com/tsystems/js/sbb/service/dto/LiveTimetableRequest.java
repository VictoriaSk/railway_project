package com.tsystems.js.sbb.service.dto;

public class LiveTimetableRequest {
    private String station;
    
	public String getStation() {
		return station;
	}
	public void setStation(String station) {
		this.station = station;
	}
}
