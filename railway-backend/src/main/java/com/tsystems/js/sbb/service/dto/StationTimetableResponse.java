package com.tsystems.js.sbb.service.dto;

import java.util.Collection;

public class StationTimetableResponse {
    private Collection<TimetableDto> trains;
    private Collection<Message> messages;

    public StationTimetableResponse(Collection<TimetableDto> trains, Collection<Message> messages) {
        this.trains = trains;
        this.messages = messages;
    }
    
    public StationTimetableResponse(Collection<Message> messages) {
		this.messages = messages;
	}

	public Collection<TimetableDto> getTrains() {
        return trains;
    }

    public Collection<Message> getMessages() {
        return messages;
    }
}

