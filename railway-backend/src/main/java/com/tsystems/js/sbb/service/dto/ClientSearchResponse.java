package com.tsystems.js.sbb.service.dto;

import java.util.Collection;

public class ClientSearchResponse {
    private Collection<PassengerDto> passengers;
    private Collection<Message> messages;
    
	public ClientSearchResponse(Collection<PassengerDto> passengers, Collection<Message> messages) {
		this.passengers = passengers;
		this.messages = messages;
	}
	
	public Collection<PassengerDto> getPassengers() {
		return passengers;
	}
	public void setPassengers(Collection<PassengerDto> passengers) {
		this.passengers = passengers;
	}
	public Collection<Message> getMessages() {
		return messages;
	}
	public void setMessages(Collection<Message> messages) {
		this.messages = messages;
	}


}

