package com.tsystems.js.sbb.service.dto;

import java.time.LocalDateTime;

public class TimetableDto {
	private String routeNum;
	private LocalDateTime arrivalTime;
	private String departureStationName;
	private LocalDateTime departureTime;
	private String arrivalStationName;
	

	public TimetableDto() {

	}

	public String getRouteNum() {
		return routeNum;
	}


	public void setRouteNum(String routeNum) {
		this.routeNum = routeNum;
	}


	public LocalDateTime getArrivalTime() {
		return arrivalTime;
	}


	public void setArrivalTime(LocalDateTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}


	public String getDepartureStationName() {
		return departureStationName;
	}


	public void setDepartureStationName(String departureStationName) {
		this.departureStationName = departureStationName;
	}


	public LocalDateTime getDepartureTime() {
		return departureTime;
	}


	public void setDepartureTime(LocalDateTime departureTime) {
		this.departureTime = departureTime;
	}


	public String getArrivalStationName() {
		return arrivalStationName;
	}


	public void setArrivalStationName(String arrivalStationName) {
		this.arrivalStationName = arrivalStationName;
	}
}

	