package com.tsystems.js.sbb.service.interfaces;

import com.tsystems.js.sbb.service.dto.BuyTicketRequest;
import com.tsystems.js.sbb.service.dto.BuyTicketResponse;

public interface TicketService {

	BuyTicketResponse buyTicket(BuyTicketRequest request);

}