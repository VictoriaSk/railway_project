package com.tsystems.js.sbb.exceptions;

import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;

public class TrainExistsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private Message message = new Message(
			"The train you are trying to add already exists. Please check out if your input data is correct.", MessageType.INFO);

	public String getMessage() {
		return message.getText();
	}
}
