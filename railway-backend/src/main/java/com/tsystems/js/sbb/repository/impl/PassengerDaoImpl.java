package com.tsystems.js.sbb.repository.impl;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tsystems.js.sbb.entity.Passenger;
import com.tsystems.js.sbb.repository.interfaces.PassengerDao;

@Repository
public class PassengerDaoImpl implements PassengerDao {

	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Passenger getById(Long passengerId) {
		@SuppressWarnings("unchecked")
		TypedQuery<Passenger> query = sessionFactory.getCurrentSession()
				.createQuery("from Passenger where passengerId = :passengerId");
		query.setParameter("passengerId", passengerId);
		return query.getSingleResult();
	}

	@Override
	public Passenger getPassenger(String passengerName, String passengerSurname, LocalDate dateOfBirth) {
		@SuppressWarnings("unchecked")
		TypedQuery<Passenger> query = sessionFactory.getCurrentSession().createQuery(
				"from Passenger where name = :passengerName and surname = :passengerSurname and dateOfBirth between :time1 and :time2");
		query.setParameter("passengerName", passengerName);
		query.setParameter("passengerSurname", passengerSurname);
		query.setParameter("time1", dateOfBirth.minusDays(1L));
		query.setParameter("time2", dateOfBirth.plusDays(1L));
		List<Passenger> passengerList = query.getResultList();
		if ((passengerList != null) && (passengerList.size()) > 0) {
			return passengerList.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public List <Passenger> getPassengersByTrainId (Long trainId) {
		@SuppressWarnings("unchecked")
		TypedQuery<Passenger> query = sessionFactory.getCurrentSession().createQuery(
				"select p from Passenger p inner join p.tickets t where t.train.trainId = :trainId");
		query.setParameter("trainId", trainId);
		return query.getResultList();
	}

	@Override
	public void save(Passenger passenger) {
		sessionFactory.getCurrentSession().save(passenger);

	}
}
