package com.tsystems.js.sbb.service.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class TrainStopAddRequest {
    private LocalDate trainDepartureDate;
	private String station;
    private String trainNumber;
    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;
    
	public LocalDate getTrainDepartureDate() {
		return trainDepartureDate;
	}
	public void setTrainDepartureDate(LocalDate trainDepartureDate) {
		this.trainDepartureDate = trainDepartureDate;
	}
	public String getStation() {
		return station;
	}
	public void setStation(String station) {
		this.station = station;
	}
	public String getTrainNumber() {
		return trainNumber;
	}
	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}
	public LocalDateTime getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(LocalDateTime departureTime) {
		this.departureTime = departureTime;
	}
	public LocalDateTime getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(LocalDateTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
 
    
}