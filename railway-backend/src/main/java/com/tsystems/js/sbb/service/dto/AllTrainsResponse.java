package com.tsystems.js.sbb.service.dto;

import java.util.Collection;

public class AllTrainsResponse {
    private Collection<TrainDto> trains;
    private Collection<Message> messages;
	
    public AllTrainsResponse(Collection<TrainDto> trains, Collection<Message> messages) {
		this.trains = trains;
		this.messages = messages;
	}
	public Collection<TrainDto> getTrains() {
		return trains;
	}
	public void setTrains(Collection<TrainDto> trains) {
		this.trains = trains;
	}
	public Collection<Message> getMessages() {
		return messages;
	}
	public void setMessages(Collection<Message> messages) {
		this.messages = messages;
	}
}

