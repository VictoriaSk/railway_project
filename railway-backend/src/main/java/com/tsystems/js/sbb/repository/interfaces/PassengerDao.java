package com.tsystems.js.sbb.repository.interfaces;

import java.time.LocalDate;
import java.util.List;

import com.tsystems.js.sbb.entity.Passenger;

public interface PassengerDao {

	Passenger getPassenger(String passengerName, String passengerSurname, LocalDate dateOfBirth);

	void save(Passenger passenger);

	Passenger getById(Long passengerId);

	List<Passenger> getPassengersByTrainId(Long trainId);

}
