package com.tsystems.js.sbb.exceptions;

import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;

public class AlreadyHasTicketException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private Message message = new Message(
			"The purchase is impossible since you already have a ticket for this journey.", MessageType.INFO);

	public String getMessage() {
		return message.getText();
	}
}
