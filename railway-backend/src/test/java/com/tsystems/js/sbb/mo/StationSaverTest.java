package com.tsystems.js.sbb.mo;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.js.sbb.entity.Station;
import com.tsystems.js.sbb.exceptions.StationExistsException;
import com.tsystems.js.sbb.repository.impl.StationDaoImpl;
import com.tsystems.js.sbb.repository.interfaces.StationDao;
import com.tsystems.js.sbb.service.dto.SaveStationRequest;
import com.tsystems.js.sbb.service.dto.StationDto;
import com.tsystems.js.sbb.service.dto.StationMapper;

public class StationSaverTest {
	
	private StationSaver sut;
	private SaveStationRequest request;
	private StationDto stationToAdd;
	private StationDao stationDaoMock;
	private List<Station> stationList;
	private StationDto stationDto;
	private StationDto result;
	
	@Before
	public void setUp() {
		stationDaoMock = Mockito.mock(StationDaoImpl.class);
		sut = new StationSaver(stationDaoMock);
		stationToAdd = new StationDto();
		stationToAdd.setStationName("Name");
		request = new SaveStationRequest(stationToAdd);
	}

	@Test
	public void testSaveSuccess() {
		Station firstStation = new Station();
		Station secondStation = new Station();
		firstStation.setStationName("First");
		secondStation.setStationName("Second");
		stationList = List.of(firstStation, secondStation);
		Mockito.when(stationDaoMock.getAll()).thenReturn(stationList);
		Mockito.doNothing().when(stationDaoMock).save(StationMapper.INSTANCE.toEntity(stationDto));
		result = sut.save(request);
		assertEquals("Name", result.getStationName());
	}
	
	@Test
	public void testSaveStationExists() {
		Station firstStation = new Station();
		Station existingStation = new Station();
		firstStation.setStationName("First");
		existingStation.setStationName("Name");
		stationList = List.of(firstStation, existingStation);
		Mockito.when(stationDaoMock.getAll()).thenReturn(stationList);
		try {
		result = sut.save(request);
		} catch (StationExistsException ex) {
			//test passed
		}
	}

}
