package com.tsystems.js.sbb.exceptions;

import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;

public class PassengersNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private Message message = new Message ("No passengers purchased a ticket for the chosen train at this point.", MessageType.INFO);

	public String getMessage() {
		return message.getText();
	}
}
