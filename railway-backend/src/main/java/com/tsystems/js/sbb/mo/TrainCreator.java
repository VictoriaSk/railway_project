package com.tsystems.js.sbb.mo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.exceptions.TrainExistsException;
import com.tsystems.js.sbb.repository.interfaces.StationDao;
import com.tsystems.js.sbb.repository.interfaces.TimetableDao;
import com.tsystems.js.sbb.repository.interfaces.TrainDao;
import com.tsystems.js.sbb.service.dto.TrainAddRequest;
import com.tsystems.js.sbb.service.dto.TrainDto;
import com.tsystems.js.sbb.service.dto.TrainMapper;
import com.tsystems.js.sbb.service.dto.TrainStopAddRequest;

@Component
public class TrainCreator {

	private final TrainDao trainDao;
	private final StationDao stationDao;
	private final TimetableDao timetableDao;

	public TrainCreator(TrainDao trainDao, StationDao stationDao, TimetableDao timetableDao) {
		this.trainDao = trainDao;
		this.stationDao = stationDao;
		this.timetableDao = timetableDao;
	}

	@Transactional
	public TrainDto saveTrain(TrainAddRequest departureInfo) throws TrainExistsException {
		String stationName = departureInfo.getStation();
		String trainNumber = departureInfo.getTrainNumber();
		LocalDateTime departureTime = departureInfo.getDepartureTime();
		Integer capacity = departureInfo.getCapacity();

		List<Train> existingTrain = trainDao.getTrainsByRouteNumberAndDepartureDate(trainNumber,
				departureTime.toLocalDate());
		if (!existingTrain.isEmpty()) {
			throw new TrainExistsException();
		} else {
			Train train = new Train(trainNumber, capacity);
			Timetable timetable = new Timetable(stationDao.getStationByName(stationName), train, departureTime);
			trainDao.save(train);
			timetableDao.save(timetable);
			return TrainMapper.INSTANCE.toDto(train, timetable, departureTime.toLocalDate());
		}
	}

	@Transactional
	public TrainDto saveTrainStop(TrainStopAddRequest trainStopInfo) {
		String stationName = trainStopInfo.getStation();
		String trainNumber = trainStopInfo.getTrainNumber();
		LocalDateTime departureTime = trainStopInfo.getDepartureTime();
		LocalDateTime arrivalTime = trainStopInfo.getArrivalTime();
		LocalDate trainDepartureDate = trainStopInfo.getTrainDepartureDate();

		List <Train> train = trainDao.getTrainsByRouteNumberAndDepartureDate(trainNumber, trainDepartureDate);
		Timetable trainStop = new Timetable(stationDao.getStationByName(stationName), train.get(0), arrivalTime,
				departureTime);
		timetableDao.save(trainStop);
		return TrainMapper.INSTANCE.toDto(train.get(0), trainStop, trainDepartureDate);
	}

}
