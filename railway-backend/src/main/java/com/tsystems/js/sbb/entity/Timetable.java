package com.tsystems.js.sbb.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "timetable")
@Entity
public class Timetable {

	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "timetable_id")
	private Long timetableId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "station_id")
	private Station station;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "train_id")
	private Train train;

	@Column(name = "arrival_time")
	private LocalDateTime arrivalTime;

	@Column(name = "departure_time")
	private LocalDateTime departureTime;

	public Timetable() {

	}

	public Timetable(Station station, Train train, LocalDateTime arrivalTime,
			LocalDateTime departureTime) {
		super();
		this.station = station;
		this.train = train;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
	}

	public Timetable(Station station, Train train,LocalDateTime departureTime) {
		super();
		this.station = station;
		this.train = train;
		this.departureTime = departureTime;
	}
	
	public void setId(Long timetableId) {
		this.timetableId = timetableId;
	}

	public Long getId() {
		return this.timetableId;
	}

	public void setStation(Station station) {
		this.station = station;
	}

	public Station getStation() {
		return this.station;
	}

	public void setTrain(Train train) {
		this.train = train;
	}

	public Train getTrain() {
		return this.train;
	}

	public void setArrivalTime(LocalDateTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public LocalDateTime getArrivalTime() {
		return this.arrivalTime;
	}

	public void setDepartureTime(LocalDateTime departureTime) {
		this.departureTime = departureTime;
	}

	public LocalDateTime getDepartureTime() {
		return this.departureTime;
	}

}
