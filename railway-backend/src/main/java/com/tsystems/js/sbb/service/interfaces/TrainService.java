package com.tsystems.js.sbb.service.interfaces;

import com.tsystems.js.sbb.service.dto.AllTrainsResponse;
import com.tsystems.js.sbb.service.dto.TrainAddRequest;
import com.tsystems.js.sbb.service.dto.TrainAddResponse;
import com.tsystems.js.sbb.service.dto.TrainSearchRequest;
import com.tsystems.js.sbb.service.dto.TrainSearchResponse;
import com.tsystems.js.sbb.service.dto.TrainStopAddRequest;
import com.tsystems.js.sbb.service.dto.TrainStopAddResponse;

public interface TrainService {

	TrainSearchResponse searchTrains(TrainSearchRequest request);

	TrainAddResponse addTrain(TrainAddRequest request);

	TrainStopAddResponse addTrainStop(TrainStopAddRequest request);

	AllTrainsResponse displayAllTrains();
}
