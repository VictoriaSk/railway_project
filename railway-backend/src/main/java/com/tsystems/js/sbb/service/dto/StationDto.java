package com.tsystems.js.sbb.service.dto;

import java.util.Set;

public class StationDto {

	private Set<TimetableDto> stationTimetableDto;
	private String stationName;
	
	public StationDto() {
		
	}

	public Set<TimetableDto> getStationTimetableDto() {
		return stationTimetableDto;
	}


	public void setStationTimetableDto(Set<TimetableDto> stationTimetableDto) {
		this.stationTimetableDto = stationTimetableDto;
	}

	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	
	
}
