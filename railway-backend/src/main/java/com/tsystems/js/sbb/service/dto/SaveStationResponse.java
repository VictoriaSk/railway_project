package com.tsystems.js.sbb.service.dto;

import java.util.Collection;

public class SaveStationResponse {
    private StationDto stationDto;
    private Collection<Message> messages;
    
    
    public SaveStationResponse(Collection<Message> messages) {
		this.messages = messages;
	}

	public SaveStationResponse(StationDto stationDto, Collection<Message> messages) {
		this.stationDto = stationDto;
		this.messages = messages;
	}

	public StationDto getStationDto() {
		return stationDto;
	}

	public Collection<Message> getMessages() {
		return messages;
	}

	
}

