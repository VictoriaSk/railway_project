
package com.tsystems.js.sbb.service.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.tsystems.js.sbb.exceptions.InvalidTravelDateException;
import com.tsystems.js.sbb.exceptions.TrainExistsException;
import com.tsystems.js.sbb.exceptions.TrainsNotFoundException;
import com.tsystems.js.sbb.mo.TrainCreator;
import com.tsystems.js.sbb.mo.TrainDisplayer;
import com.tsystems.js.sbb.mo.TrainFinder;
import com.tsystems.js.sbb.service.dto.AllTrainsResponse;
import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;
import com.tsystems.js.sbb.service.dto.TrainAddRequest;
import com.tsystems.js.sbb.service.dto.TrainAddResponse;
import com.tsystems.js.sbb.service.dto.TrainDto;
import com.tsystems.js.sbb.service.dto.TrainSearchRequest;
import com.tsystems.js.sbb.service.dto.TrainSearchResponse;
import com.tsystems.js.sbb.service.dto.TrainStopAddRequest;
import com.tsystems.js.sbb.service.dto.TrainStopAddResponse;
import com.tsystems.js.sbb.service.interfaces.TrainService;

@Service
public class TrainServiceImpl implements TrainService {

	static final Logger logger = LogManager.getLogger(TrainServiceImpl.class);
	private final TrainFinder trainFinder;
	private final TrainCreator trainCreator;
	private final TrainDisplayer trainDisplayer;

	public TrainServiceImpl(TrainFinder trainFinder, TrainCreator trainCreator, TrainDisplayer trainDisplayer) {
		this.trainFinder = trainFinder;
		this.trainCreator = trainCreator;
		this.trainDisplayer = trainDisplayer;
	}

	@Override
	public TrainSearchResponse searchTrains(TrainSearchRequest request) {
		TrainSearchResponse response;
		try {
			Collection<TrainDto> trains = trainFinder.search(request);
			response = new TrainSearchResponse(trains, Collections.emptyList());
		} catch (TrainsNotFoundException ex) {
			response = new TrainSearchResponse(Collections.emptyList(),
					List.of(new Message(ex.getMessage(), MessageType.INFO)));
		} catch (InvalidTravelDateException ex) {
			response = new TrainSearchResponse(Collections.emptyList(),
					List.of(new Message(ex.getMessage(), MessageType.INFO)));
		} catch (Exception ex) {
			response = new TrainSearchResponse(Collections.emptyList(),
					List.of(new Message(ex.getMessage(), MessageType.TECH_ERROR)));
			logger.error("Exception in class TrainServiceImpl", ex);
		}
		return response;
	}

	@Override
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public TrainAddResponse addTrain(TrainAddRequest request) {
		TrainAddResponse response;
		try {
			TrainDto train = trainCreator.saveTrain(request);
			response = new TrainAddResponse(train, Collections.emptyList());
		} catch (TrainExistsException ex) {
			response = new TrainAddResponse(List.of(new Message(ex.getMessage(), MessageType.INFO)));
		} catch (Exception ex) {
			response = new TrainAddResponse(List.of(new Message(ex.getMessage(), MessageType.TECH_ERROR)));
			logger.error("Exception in class TrainServiceImpl", ex);
		}
		return response;
	}

	@Override
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public TrainStopAddResponse addTrainStop(TrainStopAddRequest request) {
		TrainStopAddResponse response;
		try {
			TrainDto train = trainCreator.saveTrainStop(request);
			response = new TrainStopAddResponse(train, Collections.emptyList());
		} catch (Exception ex) {
			response = new TrainStopAddResponse(List.of(new Message(ex.getMessage(), MessageType.TECH_ERROR)));
			logger.error("Exception in class TrainServiceImpl", ex);
		}
		return response;
	}

	@Override
	public AllTrainsResponse displayAllTrains() {
		AllTrainsResponse response;
		try {
			Collection<TrainDto> trains = trainDisplayer.getAll();
			response = new AllTrainsResponse(trains, Collections.emptyList());
		} catch (Exception ex) {
			response = new AllTrainsResponse(Collections.emptyList(),
					List.of(new Message(ex.getMessage(), MessageType.TECH_ERROR)));
			logger.error("Exception in class TrainServiceImpl", ex);
		}
		return response;
	}

}
