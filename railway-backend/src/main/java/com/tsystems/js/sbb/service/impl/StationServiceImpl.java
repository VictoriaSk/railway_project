package com.tsystems.js.sbb.service.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.tsystems.js.sbb.exceptions.StationExistsException;
import com.tsystems.js.sbb.mo.StationDisplayer;
import com.tsystems.js.sbb.mo.StationSaver;
import com.tsystems.js.sbb.service.dto.AllStationsResponse;
import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;
import com.tsystems.js.sbb.service.dto.SaveStationRequest;
import com.tsystems.js.sbb.service.dto.SaveStationResponse;
import com.tsystems.js.sbb.service.dto.StationDto;
import com.tsystems.js.sbb.service.interfaces.StationService;

@Service
public class StationServiceImpl implements StationService {

	static final Logger logger = LogManager.getLogger(StationServiceImpl.class);
	private final StationSaver stationSaver;
	private final StationDisplayer stationDisplayer;

	public StationServiceImpl(StationSaver stationSaver, StationDisplayer stationDisplayer) {
		this.stationSaver = stationSaver;
		this.stationDisplayer = stationDisplayer;
	}

	@Override
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public SaveStationResponse saveStation(SaveStationRequest request) {
		SaveStationResponse saveStationResponse;
		try {
			StationDto station = stationSaver.save(request);
			saveStationResponse = new SaveStationResponse(station, List.of());
		} catch (StationExistsException ex) {
			saveStationResponse = new SaveStationResponse(List.of(new Message(ex.getMessage(), MessageType.INFO)));
		} catch (Exception ex) {
			saveStationResponse = new SaveStationResponse(
					List.of(new Message(ex.getMessage(), MessageType.TECH_ERROR)));
			logger.error("Exception in class StationServiceImpl", ex);
		}
		return saveStationResponse;
	}

	@Override
	public AllStationsResponse displayAllStations() {
		AllStationsResponse response;
		try {
			Collection<StationDto> stations = stationDisplayer.getAll();
			response = new AllStationsResponse(stations, Collections.emptyList());
		} catch (Exception ex) {
			response = new AllStationsResponse(Collections.emptyList(),
					List.of(new Message(ex.getMessage(), MessageType.TECH_ERROR)));
			logger.error("Exception in class StationServiceImpl", ex);
		}
		return response;
	}

}
