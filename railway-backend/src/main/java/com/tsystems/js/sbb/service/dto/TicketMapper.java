package com.tsystems.js.sbb.service.dto;

import java.time.LocalDate;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.tsystems.js.sbb.entity.Passenger;
import com.tsystems.js.sbb.entity.Train;

@Mapper
public interface TicketMapper {

	TicketMapper INSTANCE = Mappers.getMapper(TicketMapper.class);

	@Mapping(source = "train.routeNum", target = "routeNum")
	@Mapping(source = "passenger.name", target = "name")
	@Mapping(source = "passenger.surname", target = "surname")
	@Mapping(source = "passenger.dateOfBirth", target = "dateOfBirth")
	@Mapping(source = "departureDate", target = "departureDate")
	TicketDto toDto(Train train, Passenger passenger, LocalDate departureDate);
	
}