package com.tsystems.js.sbb.service.dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.tsystems.js.sbb.entity.Passenger;

@Mapper
public interface PassengerMapper {

	PassengerMapper INSTANCE = Mappers.getMapper(PassengerMapper.class);

	@Mapping(source = "name", target = "name")
	@Mapping(source = "surname", target = "surname")
	@Mapping(source = "dateOfBirth", target = "dateOfBirth")
	PassengerDto toDto(Passenger passenger);
	
	@Mapping(source = "name", target = "name")
	@Mapping(source = "surname", target = "surname")
	@Mapping(source = "dateOfBirth", target = "dateOfBirth")
	Passenger toEntity(PassengerDto passengerDto);
}
