package com.tsystems.js.sbb.exceptions;

import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;

public class NoTrainsPassingException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private Message message = new Message ("There are no trains arriving or departing from the chosen station on input date.", MessageType.INFO);

	public String getMessage() {
		return message.getText();
	}
}
