package com.tsystems.js.sbb.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tsystems.js.sbb.service.dto.AllTrainsResponse;
import com.tsystems.js.sbb.service.dto.TrainDto;
import com.tsystems.js.sbb.service.interfaces.TrainService;

@Component
@RequestScoped
@ManagedBean
public class ViewTrainsBean {

	private TrainService trainService;

	public ViewTrainsBean() {
	}

	@Autowired
	public void setTrainService(TrainService trainService) {
		this.trainService = trainService;
	}

	public List<UiTrainBean> getAllTrains() {
		AllTrainsResponse response = trainService.displayAllTrains();
		List<UiTrainBean> trainList = new ArrayList<>();
		if (response.getMessages().isEmpty()) {
			trainList = LocalDateTimeToStringConverter.convertTrainDateTime((List<TrainDto>) response.getTrains());
		}
		return trainList;
	}
}
