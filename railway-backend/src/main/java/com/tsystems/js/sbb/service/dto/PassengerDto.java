package com.tsystems.js.sbb.service.dto;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class PassengerDto {

	private Set<TicketDto> ticketsDto = new HashSet<TicketDto>();
	private String name;
	private String surname;
	private LocalDate dateOfBirth;

	public PassengerDto() {

	}
	
	public PassengerDto(String name, String surname, LocalDate dateOfBirth) {
		super();
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public LocalDate getDateOfBirth() {
		return this.dateOfBirth;
	}

	public Set<TicketDto> getTicketsDto() {
		return ticketsDto;
	}

	public void setTicketsDto(Set<TicketDto> ticketsDto) {
		this.ticketsDto = ticketsDto;
	}

}
