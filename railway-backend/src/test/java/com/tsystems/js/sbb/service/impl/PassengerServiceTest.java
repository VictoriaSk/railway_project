package com.tsystems.js.sbb.service.impl;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.js.sbb.exceptions.PassengersNotFoundException;
import com.tsystems.js.sbb.exceptions.TrainMissingException;
import com.tsystems.js.sbb.mo.PassengerFinder;
import com.tsystems.js.sbb.service.dto.ClientSearchRequest;
import com.tsystems.js.sbb.service.dto.ClientSearchResponse;
import com.tsystems.js.sbb.service.dto.MessageType;
import com.tsystems.js.sbb.service.dto.PassengerDto;
import com.tsystems.js.sbb.service.interfaces.PassengerService;

public class PassengerServiceTest {
	private PassengerService sut;
	private ClientSearchRequest requestMock;
	private PassengerFinder passengerFinderMock;

	@Before
	public void setUp() {
		passengerFinderMock = Mockito.mock(PassengerFinder.class);
		sut = new PassengerServiceImpl(passengerFinderMock);
		requestMock = new ClientSearchRequest();
	}

	@Test
	public void testSearchPassengersSuccess() {
		Mockito.when(passengerFinderMock.search(requestMock))
				.thenReturn(List.of(new PassengerDto("Test", "Test", LocalDate.now())));
		ClientSearchResponse response = sut.searchPassengers(requestMock);
		List<PassengerDto> passengers = (List<PassengerDto>) response.getPassengers();
		assertEquals(1, response.getPassengers().size());
		assertEquals("Test", passengers.get(0).getName());
		assertEquals(0, response.getMessages().size());
	}

	@Test
	public void testSearchPassengersNotFound() {
		Mockito.when(passengerFinderMock.search(requestMock)).thenThrow(new PassengersNotFoundException());
		ClientSearchResponse response = sut.searchPassengers(requestMock);
		assertEquals(0, response.getPassengers().size());
		assertEquals(1, response.getMessages().size());
		response.getMessages().forEach(message -> assertEquals(MessageType.INFO, message.type));
	}

	@Test
	public void testSearchPassengersTrainMissing() {
		Mockito.when(passengerFinderMock.search(requestMock)).thenThrow(new TrainMissingException());
		ClientSearchResponse response = sut.searchPassengers(requestMock);
		assertEquals(0, response.getPassengers().size());
		assertEquals(1, response.getMessages().size());
		response.getMessages().forEach(message -> assertEquals(MessageType.INFO, message.type));
	}

	@Test
	public void testSearchPassengersExceptionThrown() {
		Mockito.when(passengerFinderMock.search(requestMock)).thenThrow(new NullPointerException());
		ClientSearchResponse response = sut.searchPassengers(requestMock);
		assertEquals(0, response.getPassengers().size());
		assertEquals(1, response.getMessages().size());
		response.getMessages().forEach(message -> assertEquals(MessageType.TECH_ERROR, message.type));
	}
}
