package com.tsystems.js.sbb.repository.interfaces;

import com.tsystems.js.sbb.entity.Ticket;

public interface TicketDao {

	void save(Ticket ticket);
}
