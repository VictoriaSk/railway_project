package com.tsystems.js.sbb.service.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.tsystems.js.sbb.exceptions.PassengersNotFoundException;
import com.tsystems.js.sbb.exceptions.TrainMissingException;
import com.tsystems.js.sbb.mo.PassengerFinder;
import com.tsystems.js.sbb.service.dto.ClientSearchRequest;
import com.tsystems.js.sbb.service.dto.ClientSearchResponse;
import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;
import com.tsystems.js.sbb.service.dto.PassengerDto;
import com.tsystems.js.sbb.service.interfaces.PassengerService;

@Service
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class PassengerServiceImpl implements PassengerService {
	
	static final Logger logger = LogManager.getLogger(PassengerServiceImpl.class);
	private final PassengerFinder passengerFinder;

	public PassengerServiceImpl(PassengerFinder passengerFinder) {
		this.passengerFinder = passengerFinder;
	}

	@Override
	public ClientSearchResponse searchPassengers(ClientSearchRequest request) {
		ClientSearchResponse response;
		try {
			Collection<PassengerDto> passengers = passengerFinder.search(request);
			response = new ClientSearchResponse(passengers, Collections.emptyList());
		} catch (PassengersNotFoundException ex) {
			response = new ClientSearchResponse(Collections.emptyList(), List.of(new Message(ex.getMessage(), MessageType.INFO)));
		} catch (TrainMissingException ex) {
			response = new ClientSearchResponse(Collections.emptyList(), List.of(new Message(ex.getMessage(), MessageType.INFO)));
		} catch (Exception ex) {
			response = new ClientSearchResponse(Collections.emptyList(), List.of(new Message(ex.getMessage(), MessageType.TECH_ERROR)));
			logger.error("Exception in class PassengerServiceImpl", ex);
		}
		return response;
	}

}
