package com.tsystems.js.sbb.mo;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.js.sbb.entity.Station;
import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.exceptions.TrainExistsException;
import com.tsystems.js.sbb.repository.impl.StationDaoImpl;
import com.tsystems.js.sbb.repository.impl.TimetableDaoImpl;
import com.tsystems.js.sbb.repository.impl.TrainDaoImpl;
import com.tsystems.js.sbb.repository.interfaces.StationDao;
import com.tsystems.js.sbb.repository.interfaces.TimetableDao;
import com.tsystems.js.sbb.repository.interfaces.TrainDao;
import com.tsystems.js.sbb.service.dto.TrainAddRequest;
import com.tsystems.js.sbb.service.dto.TrainDto;
import com.tsystems.js.sbb.service.dto.TrainStopAddRequest;

public class TrainCreatorTest {
	private TrainCreator sut;
	private TrainDao trainDaoMock;
	private StationDao stationDaoMock;
	private TimetableDao timetableDaoMock;
	private TrainAddRequest request;
	private String stationName;
	private String trainNumber;
	private Integer capacity;
	private LocalDateTime departureTime;
	private LocalDateTime arrivalTime;
	private LocalDate trainDepartureDate;

	@Before
	public void setUp() {
		trainDaoMock = Mockito.mock(TrainDaoImpl.class);
		stationDaoMock = Mockito.mock(StationDaoImpl.class);
		timetableDaoMock = Mockito.mock(TimetableDaoImpl.class);
		sut = new TrainCreator(trainDaoMock, stationDaoMock, timetableDaoMock);
		stationName = "Test";
		trainNumber = "03";
		capacity = 100;
		departureTime = LocalDateTime.of(2020, 01, 01, 0, 0);
		arrivalTime = LocalDateTime.of(2020, 01, 01, 23, 59);
		trainDepartureDate = LocalDate.of(2020, 01, 01);
	}

	@Test
	public void testSaveTrainSuccess() {
		request = new TrainAddRequest();
		request.setDepartureTime(departureTime);
		request.setStation(stationName);
		request.setTrainNumber(trainNumber);
		request.setCapacity(capacity);
		Train train = new Train (trainNumber, capacity);
		Timetable timetable = new Timetable();
		List<Train> existingTrain = Collections.emptyList();
		Mockito.doReturn(existingTrain).when(trainDaoMock).getTrainsByRouteNumberAndDepartureDate(trainNumber,
				departureTime.toLocalDate());
		Station station = new Station();
		station.setStationName(stationName);
		Mockito.when(stationDaoMock.getStationByName(stationName)).thenReturn(station);
		Mockito.doNothing().when(trainDaoMock).save(train);
		Mockito.doNothing().when(timetableDaoMock).save(timetable);
		TrainDto result = sut.saveTrain(request);
		assertEquals(trainDepartureDate, result.getDate());
		assertEquals(stationName, result.getDepartureStation());
		assertEquals(trainNumber, result.getRouteNum());
	}
	
	@Test
	public void testSaveTrainExists() {
		request = new TrainAddRequest();
		request.setDepartureTime(departureTime);
		request.setStation(stationName);
		request.setTrainNumber(trainNumber);
		request.setCapacity(capacity);
		Train train = new Train (trainNumber, capacity);
		List<Train> existingTrain = List.of(train);
		Mockito.doReturn(existingTrain).when(trainDaoMock).getTrainsByRouteNumberAndDepartureDate(trainNumber,
				departureTime.toLocalDate());
		try{
			sut.saveTrain(request);
		} catch (TrainExistsException ex) {
			// test passed
		}
	}

	@Test
	public void testSaveTrainStopSuccess() {
		TrainStopAddRequest request = new TrainStopAddRequest();
		request.setArrivalTime(arrivalTime);
		request.setDepartureTime(departureTime);
		request.setStation(stationName);
		request.setTrainDepartureDate(trainDepartureDate);
		request.setTrainNumber(trainNumber);
		Train train = new Train (trainNumber, capacity);
		List<Train> existingTrain = List.of(train);
		Mockito.doReturn(existingTrain).when(trainDaoMock).getTrainsByRouteNumberAndDepartureDate(trainNumber,
				trainDepartureDate);
		Station station = new Station();
		station.setStationName(stationName);
		Mockito.when(stationDaoMock.getStationByName(stationName)).thenReturn(station);
		Timetable timetable = new Timetable();
		Mockito.doNothing().when(timetableDaoMock).save(timetable);
		TrainDto result = sut.saveTrainStop(request);
		assertEquals(trainDepartureDate, result.getDate());
		assertEquals(stationName, result.getDepartureStation());
		assertEquals(trainNumber, result.getRouteNum());
		assertEquals(arrivalTime, result.getArrivalTime());
		assertEquals(departureTime, result.getDepartureTime());
	}
}
