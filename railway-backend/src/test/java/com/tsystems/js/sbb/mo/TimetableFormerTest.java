package com.tsystems.js.sbb.mo;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.js.sbb.entity.Station;
import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.exceptions.NoTrainsPassingException;
import com.tsystems.js.sbb.repository.impl.TimetableDaoImpl;
import com.tsystems.js.sbb.repository.interfaces.TimetableDao;
import com.tsystems.js.sbb.service.dto.LiveTimetableRequest;
import com.tsystems.js.sbb.service.dto.StationTimetableRequest;
import com.tsystems.js.sbb.service.dto.TimetableDto;

public class TimetableFormerTest {

	private TimetableFormer sut;
	private StationTimetableRequest request;
	private LiveTimetableRequest liveRequest;
	private String stationName;
	private LocalDate date;
	private TimetableDao timetableDaoMock;

	@Before
	public void setUp() {
		timetableDaoMock = Mockito.mock(TimetableDaoImpl.class);
		sut = new TimetableFormer(timetableDaoMock);
		stationName = "TestStation";
		date = LocalDate.now();
	}

	@Test
	public void testSearchSuccessSomeTimetables() {
		request = new StationTimetableRequest();
		request.setDate(date);
		request.setStation(stationName);
		Mockito.when(timetableDaoMock.getTimetablesByStationNameAndDate(stationName, date))
				.thenReturn(getTestTimetables());
		Collection<TimetableDto> result = sut.search(request);
		List<TimetableDto> resultList = (List<TimetableDto>) result;
		assertEquals(2, result.size());
		assertEquals("SecondStation", resultList.get(1).getDepartureStationName());
		assertEquals("TestStation", resultList.get(1).getArrivalStationName());
		assertEquals("TestStation", resultList.get(0).getDepartureStationName());
		assertEquals("FirstStation", resultList.get(0).getArrivalStationName());
	}

	@Test
	public void testSearchSuccessOneTimetable() {
		request = new StationTimetableRequest();
		request.setDate(date);
		request.setStation(stationName);
		Mockito.when(timetableDaoMock.getTimetablesByStationNameAndDate(stationName, date))
				.thenReturn(getOneTestTimetable());
		Collection<TimetableDto> result = sut.search(request);
		List<TimetableDto> resultList = (List<TimetableDto>) result;
		assertEquals(1, result.size());
		assertEquals(LocalDateTime.of(2020, 5, 23, 11, 00), resultList.get(0).getArrivalTime());
		assertEquals("TestStation", resultList.get(0).getArrivalStationName());
		assertEquals("DepartureStation", resultList.get(0).getDepartureStationName());
	}

	@Test
	public void testSearchNoTrains() {
		request = new StationTimetableRequest();
		request.setDate(date);
		request.setStation(stationName);
		Mockito.when(timetableDaoMock.getTimetablesByStationNameAndDate(stationName, date))
				.thenReturn(Collections.emptyList());
		try {
			sut.search(request);
		} catch (NoTrainsPassingException ex) {
			// test passed
		}
	}

	@Test
	public void testLiveSearchSuccessSomeTimetables() {
		liveRequest = new LiveTimetableRequest();
		liveRequest.setStation(stationName);
		Mockito.when(timetableDaoMock.getTimetablesByStationNameAndCurrentTime(stationName))
				.thenReturn(getTestTimetables());
		Collection<TimetableDto> result = sut.liveSearch(liveRequest);
		List<TimetableDto> resultList = (List<TimetableDto>) result;
		assertEquals(2, result.size());
		assertEquals("SecondStation", resultList.get(1).getDepartureStationName());
		assertEquals("TestStation", resultList.get(1).getArrivalStationName());
		assertEquals("TestStation", resultList.get(0).getDepartureStationName());
		assertEquals("FirstStation", resultList.get(0).getArrivalStationName());
	}
	
	@Test
	public void testLiveSearchSuccessOneTimetable() {
		liveRequest = new LiveTimetableRequest();
		liveRequest.setStation(stationName);
		Mockito.when(timetableDaoMock.getTimetablesByStationNameAndCurrentTime(stationName))
				.thenReturn(getOneTestTimetable());
		Collection<TimetableDto> result = sut.liveSearch(liveRequest);
		List<TimetableDto> resultList = (List<TimetableDto>) result;
		assertEquals(1, result.size());
		assertEquals(LocalDateTime.of(2020, 5, 23, 11, 00), resultList.get(0).getArrivalTime());
		assertEquals("TestStation", resultList.get(0).getArrivalStationName());
		assertEquals("DepartureStation", resultList.get(0).getDepartureStationName());
	}
	
	@Test
	public void testLiveSearchNoTrains() {
		liveRequest = new LiveTimetableRequest();
		liveRequest.setStation(stationName);
		Mockito.when(timetableDaoMock.getTimetablesByStationNameAndCurrentTime(stationName))
				.thenReturn(Collections.emptyList());
		try {
			sut.liveSearch(liveRequest);
		} catch (NoTrainsPassingException ex) {
			// test passed
		}
	}

	private List<Timetable> getTestTimetables() {
		Train firstTrain = new Train();
		Timetable firstDeparture = new Timetable();
		Station firstDepartureStation = new Station();
		firstDepartureStation.setStationName("TestStation");
		firstDeparture.setStation(firstDepartureStation);
		firstDeparture.setDepartureTime(LocalDateTime.of(2020, 5, 23, 8, 00));

		Timetable firstArrival = new Timetable();
		Station firstArrivalStation = new Station();
		firstArrivalStation.setStationName("FirstStation");
		firstArrival.setStation(firstArrivalStation);
		firstArrival.setArrivalTime(LocalDateTime.of(2020, 5, 23, 10, 00));

		Set<Timetable> firstTrainTimetables = new HashSet<Timetable>();
		firstTrainTimetables.add(firstDeparture);
		firstTrainTimetables.add(firstArrival);
		firstTrain.setTrainTimetable(firstTrainTimetables);

		Train secondTrain = new Train();
		Timetable secondDeparture = new Timetable();
		Station secondDepartureStation = new Station();
		secondDepartureStation.setStationName("SecondStation");
		secondDeparture.setStation(secondDepartureStation);
		secondDeparture.setDepartureTime(LocalDateTime.of(2020, 5, 23, 9, 00));

		Timetable secondArrival = new Timetable();
		Station secondArrivalStation = new Station();
		secondArrivalStation.setStationName("TestStation");
		secondArrival.setStation(secondArrivalStation);
		secondArrival.setArrivalTime(LocalDateTime.of(2020, 5, 23, 12, 00));

		Set<Timetable> secondTrainTimetables = new HashSet<Timetable>();
		secondTrainTimetables.add(secondDeparture);
		secondTrainTimetables.add(secondArrival);
		secondTrain.setTrainTimetable(secondTrainTimetables);

		secondArrival.setTrain(secondTrain);
		firstArrival.setTrain(firstTrain);
		return List.of(firstArrival, secondArrival);
	}

	private List<Timetable> getOneTestTimetable() {
		Train train = new Train();
		Timetable departure = new Timetable();
		Station departureStation = new Station();
		departureStation.setStationName("DepartureStation");
		departure.setStation(departureStation);
		departure.setDepartureTime(LocalDateTime.of(2020, 5, 23, 9, 00));

		Timetable arrival = new Timetable();
		Station arrivalStation = new Station();
		arrivalStation.setStationName("TestStation");
		arrival.setStation(arrivalStation);
		arrival.setArrivalTime(LocalDateTime.of(2020, 5, 23, 11, 00));

		Set<Timetable> trainTimetables = new HashSet<Timetable>();
		trainTimetables.add(departure);
		trainTimetables.add(arrival);
		train.setTrainTimetable(trainTimetables);

		arrival.setTrain(train);
		return List.of(arrival);
	}
}
