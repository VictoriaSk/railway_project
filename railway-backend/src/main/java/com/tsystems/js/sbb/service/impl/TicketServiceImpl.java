package com.tsystems.js.sbb.service.impl;

import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import com.tsystems.js.sbb.exceptions.AlreadyHasTicketException;
import com.tsystems.js.sbb.mo.TicketBuyer;
import com.tsystems.js.sbb.service.dto.BuyTicketRequest;
import com.tsystems.js.sbb.service.dto.BuyTicketResponse;
import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;
import com.tsystems.js.sbb.service.dto.TicketDto;
import com.tsystems.js.sbb.service.interfaces.TicketService;

@Service
public class TicketServiceImpl implements TicketService {

	static final Logger logger = LogManager.getLogger(TicketServiceImpl.class);
	private final TicketBuyer ticketBuyer;

	public TicketServiceImpl(TicketBuyer ticketBuyer) {
		this.ticketBuyer = ticketBuyer;
	}

	@Override
	public BuyTicketResponse buyTicket(BuyTicketRequest request) {
		BuyTicketResponse buyTicketResponse;
		try {
			TicketDto ticket = ticketBuyer.buy(request);
			buyTicketResponse = new BuyTicketResponse(ticket, Collections.emptyList());
		} catch (AlreadyHasTicketException ex) {
			buyTicketResponse = new BuyTicketResponse(List.of(new Message(ex.getMessage(), MessageType.INFO)));
		} catch (Exception ex) {
			buyTicketResponse = new BuyTicketResponse(List.of(new Message(ex.getMessage(), MessageType.TECH_ERROR)));
			logger.error("Exception in class TicketServiceImpl", ex);
		}
		return buyTicketResponse;
	}

}
