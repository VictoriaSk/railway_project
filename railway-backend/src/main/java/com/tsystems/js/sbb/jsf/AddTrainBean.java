package com.tsystems.js.sbb.jsf;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.tsystems.js.sbb.service.dto.AllStationsResponse;
import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;
import com.tsystems.js.sbb.service.dto.StationDto;
import com.tsystems.js.sbb.service.dto.TrainAddRequest;
import com.tsystems.js.sbb.service.dto.TrainAddResponse;
import com.tsystems.js.sbb.service.dto.TrainStopAddRequest;
import com.tsystems.js.sbb.service.dto.TrainStopAddResponse;
import com.tsystems.js.sbb.service.interfaces.StationService;
import com.tsystems.js.sbb.service.interfaces.TrainService;

@Component
@RequestScoped
@ManagedBean
public class AddTrainBean {

	private StationService stationService;
	private TrainService trainService;

	private String error;
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss zzz uuuu", Locale.ROOT);

	@ManagedProperty("#{param.capacity}")
	private String capacity;

	@ManagedProperty("#{param.depTime}")
	private String departureTime;

	@ManagedProperty("#{param.depStation}")
	private String departureStation;

	@ManagedProperty("#{param.arrTime}")
	private String arrivalTime;

	@ManagedProperty("#{param.routeNum}")
	private String trainNumber;

	@ManagedProperty("#{param.depDate}")
	private String departureDate;

	private boolean flag;

	public AddTrainBean() {
	}

	@Autowired
	public void setStationService(StationService stationService) {
		this.stationService = stationService;
	}

	@Autowired
	public void setTrainService(TrainService trainService) {
		this.trainService = trainService;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public List<String> displayStations(String query) {
		AllStationsResponse stationsResponse = stationService.displayAllStations();
		List<String> results = new ArrayList<>();
		if (stationsResponse.getMessages().isEmpty()) {
			List<StationDto> stationList = (List<StationDto>) stationsResponse.getStations();
			stationList.forEach(station -> {
				if (StringUtils.startsWith(station.getStationName(), query)) {
					results.add(station.getStationName());
				}
			});
		}
		return results;
	}

	public String saveTrain() {
		error = "";
		TrainAddRequest trainAddRequest = new TrainAddRequest();
		trainAddRequest.setTrainNumber(trainNumber);
		trainAddRequest.setCapacity(Integer.parseInt(capacity));
		trainAddRequest.setDepartureTime(LocalDateTime.parse(departureTime, formatter));
		trainAddRequest.setStation(departureStation);
		TrainAddResponse response = trainService.addTrain(trainAddRequest);

		if (response.getMessages().isEmpty()) {
			departureDate = response.getTrain().getDate().toString();
			trainNumber = response.getTrain().getRouteNum();
		} else {
			for (Message message : response.getMessages()) {
				if (message.type.equals(MessageType.INFO)) {
					error = message.getText();
				} else {
					error = "Some technical error has occured. Please try again later.";
				}
			}
		}
		departureStation = "";
		departureTime = "";
		return "addTrain";
	}

	public String saveTrainStop() {
		error = "";
		if (StringUtils.isEmpty(departureTime)) {
			error = "Departure time can't be empty if the train has a next stop. Please fill in departure time value.";
			arrivalTime = "";
			return "addTrain";
		}
		TrainStopAddRequest request = new TrainStopAddRequest();
		request.setStation(departureStation);
		request.setTrainNumber(trainNumber);
		if (!StringUtils.isEmpty(departureTime)) {
			request.setDepartureTime(LocalDateTime.parse(departureTime, formatter));
		}
		request.setArrivalTime(LocalDateTime.parse(arrivalTime, formatter));
		request.setTrainDepartureDate(LocalDate.parse(departureDate));
		TrainStopAddResponse response = trainService.addTrainStop(request);

		if (!response.getMessages().isEmpty()) {
			error = "Some technical error has occured. Please try again later.";
		}
		departureStation = "";
		departureTime = "";
		arrivalTime = "";
		return "addTrain";
	}

	public String saveAndExit() {
		error = "";
		if (StringUtils.isNotEmpty(departureTime)) {
			error = "Departure time must be empty if current stop is the last one.";
			departureTime = "";
			arrivalTime = "";
			return "addTrain";
		}
		TrainStopAddRequest request = new TrainStopAddRequest();
		request.setStation(departureStation);
		request.setTrainNumber(trainNumber);
		request.setArrivalTime(LocalDateTime.parse(arrivalTime, formatter));
		request.setTrainDepartureDate(LocalDate.parse(departureDate));
		TrainStopAddResponse response = trainService.addTrainStop(request);

		if (response.getMessages().isEmpty()) {
			flag = true;
		} else {
			error = "Some technical error has occured. Please try again later.";
		}
		return "addTrain";
	}

	public String refreshPage() {
		flag = false;
		departureDate = StringUtils.EMPTY;
		trainNumber = StringUtils.EMPTY;
		capacity = StringUtils.EMPTY;
		departureStation = StringUtils.EMPTY;
		arrivalTime = StringUtils.EMPTY;
		return "addTrain";
	}
}
