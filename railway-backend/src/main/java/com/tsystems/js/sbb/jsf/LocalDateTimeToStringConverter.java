package com.tsystems.js.sbb.jsf;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.tsystems.js.sbb.service.dto.TimetableDto;
import com.tsystems.js.sbb.service.dto.TrainDto;

public class LocalDateTimeToStringConverter {

	public static List<UiTimetableBean> convertDateTime(List<TimetableDto> listToConvert) {
		List<UiTimetableBean> trainList = new ArrayList<>();
		listToConvert.forEach(timetable -> {
			UiTimetableBean train = new UiTimetableBean();
			train.setRouteNum(timetable.getRouteNum());
			train.setDepartureStationName(timetable.getDepartureStationName());
			train.setArrivalStationName(timetable.getArrivalStationName());
			DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm", Locale.ROOT);
			if (timetable.getDepartureTime() != null) {
				train.setDepartureTime(timetable.getDepartureTime().format(dtFormatter));
			}
			if (timetable.getArrivalTime() != null) {
				train.setArrivalTime(timetable.getArrivalTime().format(dtFormatter));
			}
			trainList.add(train);
		});
		return trainList;
	}

	public static List<UiTimetableBean> convertTime(List<TimetableDto> listToConvert) {
		List<UiTimetableBean> trainList = new ArrayList<>();
		listToConvert.forEach(timetable -> {
			UiTimetableBean train = new UiTimetableBean();
			train.setRouteNum(timetable.getRouteNum());
			train.setDepartureStationName(timetable.getDepartureStationName());
			train.setArrivalStationName(timetable.getArrivalStationName());
			DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("HH:mm", Locale.ROOT);
			if (timetable.getDepartureTime() != null) {
				train.setDepartureTime(timetable.getDepartureTime().format(dtFormatter));
			}
			if (timetable.getArrivalTime() != null) {
				train.setArrivalTime(timetable.getArrivalTime().format(dtFormatter));
			}
			trainList.add(train);
		});
		return trainList;
	}
	
	public static List<UiTrainBean> convertTrainDateTime(List<TrainDto> listToConvert) {
		List<UiTrainBean> trainList = new ArrayList<>();
		DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm", Locale.ROOT);
		listToConvert.forEach(trainDto -> {
			UiTrainBean train = new UiTrainBean();
			train.setRouteNum(trainDto.getRouteNum());
			train.setDepartureStation(trainDto.getDepartureStation());
			train.setArrivalStation(trainDto.getArrivalStation());
			train.setDepartureTime(trainDto.getDepartureTime().format(dtFormatter));
			train.setArrivalTime(trainDto.getArrivalTime().format(dtFormatter));
			train.setNumberOfTickets(trainDto.getNumberOfTickets());
			trainList.add(train);
		});
		return trainList;
	}
}
