package com.tsystems.js.sbb.repository.interfaces;

import java.time.LocalDate;
import java.util.List;

import com.tsystems.js.sbb.entity.Timetable;

public interface TimetableDao {

	void save(Timetable timetable);

	List<Timetable> getTimetablesByStationNameAndDate(String station, LocalDate date);

	List<Timetable> getTimetablesByStationNameAndCurrentTime(String station);
}
