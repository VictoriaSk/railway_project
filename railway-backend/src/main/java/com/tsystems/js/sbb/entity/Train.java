package com.tsystems.js.sbb.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "train")
@Entity
public class Train {
	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "train_id")
	private Long trainId;

	@OneToMany(mappedBy = "train", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Timetable> trainTimetable = new HashSet<Timetable>();

	@OneToMany(mappedBy = "train", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Ticket> tickets = new HashSet<Ticket>();

	@Column(name = "route_num")
	private String routeNum;

	@Column(name = "capacity")
	private Integer capacity;

	public Train() {

	}

	public Train(String routeNum, Integer capacity) {
		super();
		this.routeNum = routeNum;
		this.capacity = capacity;
	}

	public void setRouteNum(String routeNum) {
		this.routeNum = routeNum;
	}

	public String getRouteNum() {
		return this.routeNum;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Integer getCapacity() {
		return this.capacity;
	}

	public Long getTrainId() {
		return trainId;
	}

	public void setTrainId(Long trainId) {
		this.trainId = trainId;
	}

	public Set<Timetable> getTrainTimetable() {
		return trainTimetable;
	}

	public void setTrainTimetable(Set<Timetable> trainTimetable) {
		this.trainTimetable = trainTimetable;
	}

	public Set<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(Set<Ticket> tickets) {
		this.tickets = tickets;
	}

}
