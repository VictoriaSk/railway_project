package com.tsystems.js.sbb.exceptions;

import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;

public class TrainMissingException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private Message message = new Message ("Please check out the input data. The train you are looking for doesn't exist.", MessageType.INFO);

	public String getMessage() {
		return message.getText();
	}
}
