package com.tsystems.js.sbb.mo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.js.sbb.entity.Timetable;
import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.exceptions.InvalidTravelDateException;
import com.tsystems.js.sbb.exceptions.TrainsNotFoundException;
import com.tsystems.js.sbb.repository.interfaces.TrainDao;
import com.tsystems.js.sbb.service.dto.TrainDto;
import com.tsystems.js.sbb.service.dto.TrainSearchRequest;

@Component
public class TrainFinder {

	private final TrainDao trainDao;

	public TrainFinder(TrainDao trainDao) {
		this.trainDao = trainDao;
	}

	@Transactional 
	public List<TrainDto> search(TrainSearchRequest travelDetails) {
	  List<Train> trains = getTrainEntityList(travelDetails);
		List<TrainDto> resultList = new ArrayList<>();
		for (Train train : trains) {
			TrainDto result = new TrainDto();
			result.setRouteNum(train.getRouteNum());
			result.setNumberOfTickets(train.getCapacity() - train.getTickets().size());
			result.setDate(getTrainDepartureDate(train));
			train.getTrainTimetable().forEach(timetable -> {
				if (timetable.getStation().getStationName().equals(travelDetails.getFrom())) {
					result.setDepartureTime(timetable.getDepartureTime());
					result.setDepartureStation(timetable.getStation().getStationName());
				}
				if (timetable.getStation().getStationName().equals(travelDetails.getTo())) {
					result.setArrivalTime(timetable.getArrivalTime());
					result.setArrivalStation(timetable.getStation().getStationName());
				}
			});
			resultList.add(result);
		}
		return sortByDepartureTime(resultList);
	}

	@Transactional 
	public List<Train> getTrainEntityList(TrainSearchRequest travelDetails) throws TrainsNotFoundException, InvalidTravelDateException {
		LocalDate travelDate = travelDetails.getTravelDate();
		String departureStation = travelDetails.getFrom();
		String arrivalStation = travelDetails.getTo();
		LocalDateTime travelTimeStart;
		if (travelDate.isBefore(LocalDate.now())) {
			throw new InvalidTravelDateException();
		}
		if (travelDate.isEqual(LocalDate.now())) {
			travelTimeStart = LocalDateTime.now().withSecond(0).withNano(0).plusMinutes(10);
		} else {
			travelTimeStart = travelDate.atStartOfDay();
		}
		LocalDateTime travelTimeEnd = travelDate.atTime(23, 59);
		LocalDateTime timeOfArrivalEnd = travelTimeEnd.plusDays(10L);
		List<Train> trains = getTrainsByStationNamesAndTravelDate(departureStation, arrivalStation, travelTimeStart,
				travelTimeEnd, timeOfArrivalEnd);
		if (trains.isEmpty()) {
			throw new TrainsNotFoundException();
		}
		return trains;
	}

	@Transactional 
	public List<Train> getTrainsByStationNamesAndTravelDate(String departureStation, String arrivalStation,
			LocalDateTime travelTimeStart, LocalDateTime travelTimeEnd, LocalDateTime timeOfArrivalEnd) {
		List<Timetable> departureTimetables = trainDao
				.getTimetablesByDepartureStationNameAndDepartureDate(departureStation, travelTimeStart, travelTimeEnd);
		List<Timetable> arrivalTimetables = trainDao.getTimetablesByArrivalStationNameAndArrivalPeriod(arrivalStation,
				travelTimeStart, timeOfArrivalEnd);
		List<Train> result = new ArrayList<Train>();
		departureTimetables.forEach(departureTimetable -> {
			arrivalTimetables.forEach(arrivalTimetable -> {
				if (departureTimetable.getTrain().getTrainId().equals(arrivalTimetable.getTrain().getTrainId())) {
					Train train = trainDao.getById(departureTimetable.getTrain().getTrainId());
					if (hasAvailableTickets(train)) {
						result.add(train);
					}
				}
			});
		});
		return result;
	}

	private LocalDate getTrainDepartureDate(Train train) {
		Set<Timetable> trainTimetables = train.getTrainTimetable();
		LocalDate departureDate = LocalDate.MAX;
		for (Timetable timetable : trainTimetables) {
			if (timetable.getDepartureTime() != null) {
				if (timetable.getDepartureTime().toLocalDate().isBefore(departureDate)) {
					departureDate = timetable.getDepartureTime().toLocalDate();
				}
			}
		}
		return departureDate;
	}

	private boolean hasAvailableTickets(Train train) {
		if (train.getCapacity() - train.getTickets().size() > 0) {
			return true;
		}
		return false;
	}
	
	private List<TrainDto> sortByDepartureTime(List<TrainDto> listToSort) {
		List<TrainDto> sortedList = listToSort.stream().sorted((a, b) -> {
			return a.getDepartureTime().compareTo(b.getDepartureTime());
		}).collect(Collectors.toList());
		return sortedList;
	}

}
