package com.tsystems.js.sbb.service.interfaces;

import com.tsystems.js.sbb.service.dto.LiveTimetableRequest;
import com.tsystems.js.sbb.service.dto.StationTimetableRequest;
import com.tsystems.js.sbb.service.dto.StationTimetableResponse;

public interface TimetableService {
	
	StationTimetableResponse getSchedule(StationTimetableRequest request);

	StationTimetableResponse getLiveSchedule(LiveTimetableRequest request);

}
