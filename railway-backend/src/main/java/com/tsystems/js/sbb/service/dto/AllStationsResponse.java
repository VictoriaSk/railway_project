package com.tsystems.js.sbb.service.dto;

import java.util.Collection;

public class AllStationsResponse {
	private Collection<StationDto> stations;
	private Collection<Message> messages;

	public AllStationsResponse(Collection<StationDto> stations, Collection<Message> messages) {
		this.stations = stations;
		this.messages = messages;
	}

	public Collection<StationDto> getStations() {
		return stations;
	}

	public void setStations(Collection<StationDto> stations) {
		this.stations = stations;
	}

	public Collection<Message> getMessages() {
		return messages;
	}

	public void setMessages(Collection<Message> messages) {
		this.messages = messages;
	}
}
