package com.tsystems.js.sbb.repository.impl;

import javax.persistence.TypedQuery;
import java.util.List;

import com.tsystems.js.sbb.entity.Station;
import com.tsystems.js.sbb.repository.interfaces.StationDao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StationDaoImpl implements StationDao {

	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void save(Station station) {
		sessionFactory.getCurrentSession().save(station);
	}

	@Override
	public List<Station> getAll() {
		@SuppressWarnings("unchecked")
		TypedQuery<Station> query = sessionFactory.getCurrentSession().createQuery("from Station");
		return query.getResultList();
	}
	
	@Override
	public Station getStationByName(String stationName) {
		@SuppressWarnings("unchecked")
		TypedQuery<Station> query = sessionFactory.getCurrentSession().createQuery("from Station where stationName = :stationName");
		query.setParameter("stationName", stationName);
		return query.getSingleResult();
	}
}
