package com.tsystems.js.sbb.mo;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.js.sbb.entity.Passenger;
import com.tsystems.js.sbb.entity.Ticket;
import com.tsystems.js.sbb.entity.Train;
import com.tsystems.js.sbb.exceptions.AlreadyHasTicketException;
import com.tsystems.js.sbb.repository.interfaces.PassengerDao;
import com.tsystems.js.sbb.repository.interfaces.TicketDao;
import com.tsystems.js.sbb.repository.interfaces.TrainDao;
import com.tsystems.js.sbb.service.dto.BuyTicketRequest;
import com.tsystems.js.sbb.service.dto.TicketDto;
import com.tsystems.js.sbb.service.dto.TicketMapper;

@Component
public class TicketBuyer {

	private final TicketDao ticketDao;
	private final TrainDao trainDao;
	private final PassengerDao passengerDao;

	public TicketBuyer(TicketDao ticketDao, TrainDao trainDao, PassengerDao passengerDao) {
		this.passengerDao = passengerDao;
		this.trainDao = trainDao;
		this.ticketDao = ticketDao;
	}

	@Transactional
	public TicketDto buy(BuyTicketRequest ticketParams) throws AlreadyHasTicketException {

		String name = ticketParams.getName();
		String surname = ticketParams.getSurname();
		LocalDate dateOfBirth = ticketParams.getDateOfBirth();
		String trainNumber = ticketParams.getTrainNumber();
		LocalDate departureDate = ticketParams.getDepartureDate();

		List <Train> chosenTrain = trainDao.getTrainsByRouteNumberAndDepartureDate(trainNumber, departureDate);
		List<Passenger> passengersOfChosenTrain = passengerDao.getPassengersByTrainId(chosenTrain.get(0).getTrainId());
		if (hasTicketOnTrain(passengersOfChosenTrain, name, surname, dateOfBirth)) {
			throw new AlreadyHasTicketException();
		}
		Passenger passenger = generateAndSavePassenger(name, surname, dateOfBirth);
		ticketDao.save(new Ticket(chosenTrain.get(0), passenger));
		return TicketMapper.INSTANCE.toDto(chosenTrain.get(0), passenger, departureDate);
	}

	private boolean hasTicketOnTrain(List<Passenger> passengers, String passengerName, String passengerSurname,
			LocalDate dateOfBirth) {
		if (passengers.isEmpty()) {
			return false;
		} else {
			for (Passenger passenger : passengers) {
				if (passenger.getName().equals(passengerName) && (passenger.getSurname().equals(passengerSurname))
						&& (passenger.getDateOfBirth().equals(dateOfBirth))) {
					return true;
				}
			}
			return false;
		}
	}
	
	@Transactional
	public Passenger generateAndSavePassenger(String name, String surname, LocalDate dateOfBirth) {
		Passenger passenger = new Passenger(name, surname, dateOfBirth);
		if (passengerDao.getPassenger(name, surname, dateOfBirth) == null) {
			passengerDao.save(passenger);
			return passenger;
		}
		else {
			return passengerDao.getPassenger(name, surname, dateOfBirth);
		}
	}
}
