package com.tsystems.js.sbb.service.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.tsystems.js.sbb.exceptions.NoTrainsPassingException;
import com.tsystems.js.sbb.mo.TimetableFormer;
import com.tsystems.js.sbb.service.dto.LiveTimetableRequest;
import com.tsystems.js.sbb.service.dto.Message;
import com.tsystems.js.sbb.service.dto.MessageType;
import com.tsystems.js.sbb.service.dto.StationTimetableRequest;
import com.tsystems.js.sbb.service.dto.StationTimetableResponse;
import com.tsystems.js.sbb.service.dto.TimetableDto;
import com.tsystems.js.sbb.service.interfaces.TimetableService;

@Service
public class TimetableServiceImpl implements TimetableService {

	static final Logger logger = LogManager.getLogger(TimetableServiceImpl.class);
	private final TimetableFormer timetableFormer;

	public TimetableServiceImpl(TimetableFormer timetableFormer) {
		this.timetableFormer = timetableFormer;
	}

	@Override
	public StationTimetableResponse getSchedule(StationTimetableRequest request) {
		StationTimetableResponse response;
		try {
			Collection<TimetableDto> trains = timetableFormer.search(request);
			response = new StationTimetableResponse(trains, Collections.emptyList());
		} catch (NoTrainsPassingException ex) {
			response = new StationTimetableResponse(List.of(new Message(ex.getMessage(), MessageType.INFO)));
		} catch (Exception ex) {
			response = new StationTimetableResponse(List.of(new Message(ex.getMessage(), MessageType.TECH_ERROR)));
			logger.error("Exception in class TimetableServiceImpl", ex);
		}
		return response;
	}
	
	@Override
	public StationTimetableResponse getLiveSchedule(LiveTimetableRequest request) {
		StationTimetableResponse response;
		try {
			Collection<TimetableDto> trains = timetableFormer.liveSearch(request);
			response = new StationTimetableResponse(trains, Collections.emptyList());
		} catch (NoTrainsPassingException ex) {
			response = new StationTimetableResponse(List.of(new Message(ex.getMessage(), MessageType.INFO)));
		} catch (Exception ex) {
			response = new StationTimetableResponse(List.of(new Message(ex.getMessage(), MessageType.TECH_ERROR)));
			logger.error("Exception in class TimetableServiceImpl", ex);
		}
		return response;
	}

}
