package com.tsystems.js.sbb.service.dto;

import java.util.Collection;

public class TrainSearchResponse {
    private Collection<TrainDto> trains;
    private Collection<Message> messages;

    public TrainSearchResponse(Collection<TrainDto> trains, Collection<Message> messages) {
        this.trains = trains;
        this.messages = messages;
    }

    public Collection<TrainDto> getTrains() {
        return trains;
    }

    public Collection<Message> getMessages() {
        return messages;
    }
}

