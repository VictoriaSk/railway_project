package com.tsystems.js.sbb.mo;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.js.sbb.entity.Station;
import com.tsystems.js.sbb.repository.impl.StationDaoImpl;
import com.tsystems.js.sbb.repository.interfaces.StationDao;
import com.tsystems.js.sbb.service.dto.StationDto;

public class StationDisplayerTest {

	private StationDisplayer sut;
	private StationDao stationDaoMock;
	private List<Station> stationList;
	private List<StationDto> resultList;

	@Before
	public void setUp() {
		stationDaoMock = Mockito.mock(StationDaoImpl.class);
		sut = new StationDisplayer(stationDaoMock);
	}

	@Test
	public void testGetAllSuccessSomeStations() {
		Station firstStation = new Station();
		Station secondStation = new Station();
		Station nextStation = new Station();
		firstStation.setStationName("First");
		secondStation.setStationName("Second");
		nextStation.setStationName("Next");
		stationList = List.of(firstStation, secondStation, nextStation);
		Mockito.when(stationDaoMock.getAll()).thenReturn(stationList);
		resultList = sut.getAll();
		assertEquals(firstStation.getStationName(), resultList.get(0).getStationName());
		assertEquals(secondStation.getStationName(), resultList.get(2).getStationName());
		assertEquals(nextStation.getStationName(), resultList.get(1).getStationName());
	}

	@Test
	public void testGetAllSuccessOneStation() {
		Station firstStation = new Station();
		firstStation.setStationName("First");
		stationList = List.of(firstStation);
		Mockito.when(stationDaoMock.getAll()).thenReturn(stationList);
		resultList = sut.getAll();
		assertEquals(firstStation.getStationName(), resultList.get(0).getStationName());
		assertEquals(1, resultList.size());
	}
}
